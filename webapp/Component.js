var oComponent_PharmCr;
sap.ui.define([
	"sap/base/util/UriParameters",
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel",
	"sap/f/library",
	"sap/f/FlexibleColumnLayoutSemanticHelper",
	"zmm_create_pharm_or/model/models",
	"sap/ui/core/IconPool",
	'sap/m/MessageBox'
], function(UriParameters, UIComponent, JSONModel, library, FlexibleColumnLayoutSemanticHelper, models, IconPool, MessageBox) {
	"use strict";

	var LayoutType = library.LayoutType;

	var Component = UIComponent.extend("zmm_create_pharm_or.Component", {
		metadata: {
			manifest: "json"
		},

		init: function() {
			UIComponent.prototype.init.apply(this, arguments);
			oComponent_PharmCr = this;
			//Set Right to Left
			// @ts-ignore
			sap.ui.getCore().getConfiguration().setRTL(true);
			//Set HE language
			// @ts-ignore
			sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
			var oModel = new JSONModel();
			this.setModel(oModel);
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			// set the FLP model
			this.setModel(models.createFLPModel(), "FLP");
			//create json model
			this.setModel(models.createJSONModel(), "JSON");
			this.getRouter().initialize();

			const aFonts = [
				{
					fontFamily: "SAP-icons-TNT",
					fontURI: sap.ui.require.toUrl("sap/tnt/themes/base/fonts/")
				},
				{
					fontFamily: "BusinessSuiteInAppSymbols",
					fontURI: sap.ui.require.toUrl("sap/ushell/themes/base/fonts/")
				}
			];
	
			aFonts.forEach(oFont => {
				IconPool.registerFont(oFont);
			});

			//busyIndicator
			this.getModel("ODATA").attachBatchRequestSent(function () {
				sap.ui.core.BusyIndicator.show();
			}).attachBatchRequestCompleted(function (event) {
				sap.ui.core.BusyIndicator.hide();
			});
			//add chrome message
			window.addEventListener('beforeunload', oComponent_PharmCr.beforeUnload, true);
			
		},
		beforeUnload: function (e) {
			e.preventDefault();
			e.returnValue = '';
		},
		destroy: function () {
			window.removeEventListener('beforeunload', oComponent_PharmCr.beforeUnload, true);
			// var cartCount = oComponent_PharmCr.getModel("JSON").getProperty("/CartCount");
			// if(!!cartCount){
			// 	MessageBox.alert("קיימים פריטים בסל, האם תרצה לצאת מבלי לשמור טיוטה או לסיים את ההזמנה", {
			// 		actions: ['לא', 'כן'],
			// 		emphasizedAction: 'כן',
			// 		onClose: function (sAction) {
			// 			if (sAction === 'כן') { 
			// 				UIComponent.prototype.destroy.apply(this, arguments);
			// 				return;
			// 			}else{
			// 				return;
			// 			}
			// 		}
			// 	});
			// }
			UIComponent.prototype.destroy.apply(this, arguments);

		},

		/**
		 * Returns an instance of the semantic helper
		 * @returns {sap.f.FlexibleColumnLayoutSemanticHelper} An instance of the semantic helper
		 */
		getHelper: function() {
			var oFCL = this.getRootControl().byId("fcl"),
				// oParams = UriParameters.fromQuery(location.search),
				oSettings = {
					defaultTwoColumnLayoutType: LayoutType.TwoColumnsMidExpanded,
					defaultThreeColumnLayoutType: LayoutType.ThreeColumnsMidExpandedEndHidden,
					mode: "ThreeColumnsMidExpandedEndHidden",
					initialColumnsCount: 3,
					maxColumnsCount: 3
				};

			return FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings);
		},
		i18n: function (str) {
			return oComponent_PharmCr.getModel("i18n").getProperty(str);
		}
	});
	return Component;
});