/*global true oComponent_PharmCr*/
sap.ui.define([], function() {
	"use strict";

	return {
		getMaterialIcon: function(sMatType){
			if (sMatType === 'ZPRP'){
				return 'sap-icon://BusinessSuiteInAppSymbols/icon-medicine-pill';
			}
			if(sMatType === 'ZSPN'){
				return 'sap-icon://stethoscope';
			}
		},
		getMaterialIconText: function(sMatType){
			if (sMatType === 'ZPRP'){
				return oComponent_PharmCr.i18n('MedPreparation');
			}
			if(sMatType === 'ZSPN'){
				return oComponent_PharmCr.i18n('ConMedicalEqu');
			}
		},
		getMessageType: function (errorType) {
			if (errorType === 'E') {
				return 'Error';
			} else if (errorType === 'W') {
				return 'Warning';
			} else if (errorType === 'S') {
				return 'Success';
			} else {
				return 'None';
			}
		},
		getValueState: function (OrderQuan, Bstrf, Maxqut, btnClick ) {
			if( ( 
					(!!( Number(OrderQuan) / Number(Bstrf) ).toString().split(".")[1] && Number(Bstrf)) 
					|| 
					( Number(OrderQuan) > Number(Maxqut) && Number(Maxqut)) 
				)
				// && btnClick
			  ){
				return 'Error';
			}else{
				return 'None';
			}
		}

	};

});