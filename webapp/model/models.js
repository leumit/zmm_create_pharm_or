/* global oComponent_PharmCr : true */
/* global isAttachRequest : true */
/* global oModels : true */
/* global OWATracker: true */
/* global trackingPlugin: true */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/ui/model/Filter",
	"sap/m/MessageBox"
], function(JSONModel, Device, Filter, MessageBox) {
	"use strict";

	return {
		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createFLPModel: function() {
			var fnGetuser = jQuery.sap.getObject("sap.ushell.Container.getUser"),
				bIsShareInJamActive = fnGetuser ? fnGetuser().isJamActive() : false,
				oModel = new JSONModel({
					isShareInJamActive: bIsShareInJamActive
				});
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				bSuccCreatOrder: false,
				bSuccessReleaseAndNote: false,
				editCart: false,
				addTocartClicked:false,
				addAllSelectedToCart: false,
				Routes: {
					SelectedTab: 'MaterialDetails'
				},
				fromDeleteDraft: false,
				isOrderComment: false,
				FavoriteClick: false,
				TodayDate: new Date(),
				IconTabBarKey: "RecItems",
				radioBtnAtc: 0,
				radioBtnPeriod: 1,
				CartCount: 0,
				approveGenClicked : false,
				addToCartClicked: false,
				saveChangeInCart: false,
				SelectedAll: false,
				SelectedAllGen: false,
				allTabSelected: false,
				genericClick: false,
				deletedItems: [],
				textPeriod: oComponent_PharmCr.i18n('LastMonth'),
				deleteMultiMatrialVisible: false,
				deleteMultiABCVisible: false,
				Filters: {
					Berid:"",
					Bertx: "",
					freeSearch: '',
					MateiralType: '1',
					AbcKeys: ["A"],
					DrugsKey : '1',
					CoolKey : '1',
					StockDayKey: 'N',
					ShowBlockMat: true,
					ShowRecNupko: false,
					BeginDateTxt: String(new Date(new Date().setDate(new Date().getDate() - 30)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear()),
					BeginDate: new Date(new Date().setDate(new Date().getDate() - 30)),
					EndDate: new Date(),
					EndDateTxt: String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear())
				},
				cartFilters: {
					freeSearch: ''
				},
				layout: 'OneColumn',
				DrugsItems:
				[
					{value: oComponent_PharmCr.i18n("All") , key:'1'},
					{value: oComponent_PharmCr.i18n("Drugs")  , key:'2'},
					{value: oComponent_PharmCr.i18n("NoDrugs") , key:'3'}
				],
				MaterialTypesItems:
				[
					{value: oComponent_PharmCr.i18n("All") , key:'1'},
					{value:oComponent_PharmCr.i18n("MedPreparation") , key:'2'},
					{value:oComponent_PharmCr.i18n("ConMedicalEqu") , key:'3'}
				],
				ABCItems:[],
				CoolItems:
				[
					{value: oComponent_PharmCr.i18n("All") , key:'1'},
					{value:oComponent_PharmCr.i18n("2_8") , key:'2'},
					{value:oComponent_PharmCr.i18n("Cytotoxicity") , key:'3'},
					{value:oComponent_PharmCr.i18n("NoCool") , key:'4'}
				],
				StockDaysItems:[]	
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		handleErrors: function (error) {
			try {
				MessageBox.error(JSON.parse(error.responseText).error.message.value);
			} catch (e) {
				rror(JSON.stringify(error.message));
			}
		},
		getFixedDate: function (value) {
			if (!value) {
				return;
			}
			var before = value.getTime();
			var after = new Date(before + 10800000); //adding three hours to fix UTC gateway processing day back
			return after;
		},
		LoadMatDetails: function (sMaterialNumber, sLoc) {
			const sKey = oComponent_PharmCr.getModel("ODATA").createKey("/ItemDetailsSet", {
				IvMatnr: sMaterialNumber,
				IvMainSloc: sLoc
			});

			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});
		},
		LoadMrpDetails: function (sMaterialNumber, sMrpArea) {
			const sKey = oComponent_PharmCr.getModel("ODATA").createKey("/ItemMrpDataSet", {
				IvMatnr: sMaterialNumber,
				IvMrpArea: sMrpArea
			});

			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});
		},
		LoadSalesDetails: function (sFromDate, sToDate, sMaterialNumber, sLoc) {
			const sKey = oComponent_PharmCr.getModel("ODATA").createKey("/ItemSalesHDRSet", {
				IvIssueFrom: this.getFixedDate(sFromDate),
				IvIssueTo: this.getFixedDate(sToDate),
				IvMatnr: sMaterialNumber,
				IvSloc: sLoc
			});
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read(sKey, {
					urlParameters: {
						"$expand": "ItemSalesLine"
					},
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});
		},
		LoadOrderHistory: function (sMatnr, sSloc) {
			var oModel = oComponent_PharmCr.getModel("ODATA");
			const sKey = oModel.createKey("/OrdersHstOrdersHDRSet", {
				IvMatnr: sMatnr,
				IvSloc: sSloc

			});
			return new Promise((resolve, reject) => {
				oModel.read(sKey, {
					urlParameters: {
						"$expand": "OrdersHstOrders/OrdersHstGoodsRecipt"
					},
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				});
			});
		},
		UpdateFavoriteMat(sActivity, sApplicationId, sObjectId ,sObjectValue, sUserName){
			
			var oModel = oComponent_PharmCr.getModel("ZCA");

			const sKey = oModel.createKey("/UpdateFioriFavoriteSet", {
				IvActivity: sActivity,
				IvApplicationId: sApplicationId,
				IvObjectId: sObjectId,
				IvObjectValue: sObjectValue,
				IvUserName: sUserName
			});

			return new Promise((resolve, reject) => {
				oModel.read(sKey, {
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				});
			});

		},
		getAbcList: function () {
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read("/GetAbcListSet", {
					filters: [],
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
					}
				});
			});
		},
		LoadItems: function (bIvTabsel) {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				oModelData = oJsonModel.getProperty("/Filters"),
				bIsDraft = oJsonModel.getProperty("/IsDraft");

			const aFilters = [
				new sap.ui.model.Filter("IvBerid", "EQ", oModelData.Berid),
				new sap.ui.model.Filter("IvFromDate", "EQ", this.getFixedDate(oModelData.BeginDate)),
				new sap.ui.model.Filter("IvToDate", "EQ", this.getFixedDate(oModelData.EndDate)),
				new sap.ui.model.Filter("IvTabsel", "EQ", bIvTabsel)
			];

			if(!!bIsDraft){
				aFilters.push(new sap.ui.model.Filter("IvDrafts", "EQ", true));
			}
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read("/ItemsMRPDataSet", {
					filters: aFilters,
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});
		},
		CheckDraft: function () {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				oModelData = oJsonModel.getProperty("/Filters"),
				oModel = oComponent_PharmCr.getModel("ODATA");

			var sKey = oModel.createKey("/CheckDraftSet", {
				IvBerid: oModelData.Berid
			});
			return new Promise((resolve, reject) => {
				oModel.read(sKey, {
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				});
			});
		},
		LoadItemsGen: function (IvAtccode,IvMatnr) {
			var oModelData = oComponent_PharmCr.getModel("JSON").getData().Filters;
			const aFilters = [
				new sap.ui.model.Filter("IvAtccode", "EQ", IvAtccode),
				new sap.ui.model.Filter("IvMatnr", "EQ", IvMatnr),
				new sap.ui.model.Filter("IvBerid", "EQ", oModelData.Berid),
				new sap.ui.model.Filter("IvFromDate", "EQ", this.getFixedDate(oModelData.BeginDate)),
				new sap.ui.model.Filter("IvToDate", "EQ", this.getFixedDate(oModelData.EndDate))
			];
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read("/GetGenericsDataSet", {
					filters: aFilters,
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});
		},
		createOrder: function (oEntry) {
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").create("/SetPrPoSet", oEntry, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});

		},
		updateAndReleaseOrder: function (oEntry) {
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").create("/NoteAndRealeaseOrderHeaderSet", oEntry, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});

		},
		getPermissionPilot: function (sUserName) {
			const sKey = oComponent_PharmCr.getModel("ODATA").createKey("/PilotCheckSet", {
				IvUserName: sUserName
			});
			return new Promise(function (resolve, reject) {
				oComponent_PharmCr.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						//console.log(error);

					}
				});
			});
		},


	};

});