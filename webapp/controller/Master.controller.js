/* global oComponent_PharmCr: true */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/ui/model/Sorter',
	'sap/m/MessageBox',
	"zmm_create_pharm_or/controller/BaseController",
	'sap/m/MessageToast',
	"sap/m/Select",
	"zmm_create_pharm_or/model/models",
	"sap/ui/export/Spreadsheet",
	"sap/ui/dom/jquery/getSelectedText",
	'sap/ui/table/TablePersoController',

], function (JSONModel,
	Controller,
	Filter,
	FilterOperator,
	Sorter,
	MessageBox,
	BaseController,
	MessageToast,
	Select,
	models,
	Spreadsheet,
	getSelectedText,
	TablePersoController) {
	"use strict";


	return BaseController.extend("zmm_create_pharm_or.controller.Master", {
		onInit: function () {
			oComponent_PharmCr._MasterController = this;
			
			setTimeout(this.PersonalizationTable, 5000);
			// this.PersonalizationTable();
			debugger;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oRouter.getRoute("master").attachPatternMatched(this._onMasterParmMatched, this);
			this.oRouter.getRoute("").attachPatternMatched(this._onMasterMatched, this);
			this._bDescendingSort = false;
		},
		// onExit: function () {
		// 	this._oTPC.destroy();
		// },
		// PersonalizationTable: function() {
		// 	// Personalisation to dataTable
		// 	var sKey =  "zmm_create_pharm_or";
		// 	try{
		// 		oComponent_PharmCr._MasterController.oPersonalizationService = sap.ushell.Container.getService("Personalization");
		// 		var oPersId = {
		// 			container: sKey, //any
		// 			item: "dataTable" //any- I have used the table name 
		// 		};
		// 		// var oScope = {
		// 		// 	keyCategory: oComponent_PharmCr._MasterController.oPersonalizationService.constants.keyCategory.FIXED_KEY,
		// 		// 	writeFrequency: oComponent_PharmCr._MasterController.oPersonalizationService.constants.writeFrequency.LOW,
		// 		// 	clientStorageAllowed: true
		// 		// };
	
		// 		// Get a Personalizer
		// 		var oPersonalizer = oComponent_PharmCr._MasterController.oPersonalizationService.getPersonalizer(oPersId, undefined, oComponent_PharmCr);
	
		// 		oComponent_PharmCr._MasterController._oTPC = new TablePersoController({
		// 			table: oComponent_PharmCr._MasterController.getView().byId("dataTable"),
		// 			// componentName: sKey,
		// 			persoService: oPersonalizer
		// 		});

		// 	}catch (error){
		// 		console.log(error)
		// 	}
		// },	
		onAfterRendering: function() {
			oComponent_PharmCr._MasterController.overrideUiTableFilter("dataTable");  // override filter of the coulmns to be string or number - for filteropertors
		},	
		onPersoButtonPressed: function(oEvent) {
			oComponent_PharmCr._MasterController._oTPC.openDialog();
		},
		onSearch: function (oEvent) {
			var sQuery = oEvent.getParameter("newValue");

			if (sQuery && sQuery.length > 0) {
				var oFilter = new Filter({
					filters: [	new Filter('Maktx', FilterOperator.Contains, sQuery),
								new Filter('Matnr', FilterOperator.Contains, sQuery),
								new Filter('Atccode', FilterOperator.Contains, sQuery)

					],
					and: false
				});
			}
			 oComponent_PharmCr._MasterController.getView().byId("dataTable").getBinding("rows").filter(oFilter, "Application");
		},
		_onMasterMatched: function(oEvent){
			debugger;
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				items = oJsonModel.getProperty("/items"),
				FavoriteClick =	oJsonModel.getProperty("/FavoriteClick"),
				bIsDraft = oEvent.getParameter("arguments").IsDraft,
				sUserName = "";

				try {
					sUserName = new sap.ushell.services.UserInfo().getUser().getId();
				} catch (error) {
					sUserName = "SHIRIN";
					//console.log(error);
				}

			oComponent_PharmCr._fcl.byId("fcl").setLayout("OneColumn");
			oComponent_PharmCr._fcl._updateUIElements();
			
			//load abc List
			debugger;
			models.getAbcList().then((data) => {
				var jsonModel = oComponent_PharmCr.getModel("JSON"),
					aABCItems = jsonModel.getProperty("/ABCItems"),
					abcKeys = jsonModel.getProperty("/Filters/AbcKeys");
				debugger;
				if(!!data.results.length && !!data.results){
					data.results.forEach(item => {
						aABCItems.push({ value: item.AbcType, desc: item.AbcDesc });
						abcKeys.push(item.AbcType);
					});
					aABCItems.push({value:oComponent_PharmCr.i18n("EMPTY"), desc: ''});
					abcKeys.push(oComponent_PharmCr.i18n("EMPTY"));

					jsonModel.setProperty("/ABCItems", aABCItems);
					jsonModel.setProperty("/Filters/AbcKeys", abcKeys);
					jsonModel.setProperty("/AbcAllKeys", abcKeys);
					jsonModel.refresh();
				}
			}).catch((error) => {
				models.handleErrors(error);
			});
			models.getPermissionPilot(sUserName).then((data) => {			
				if(data.EvAuthorized){
					debugger;
					// oJsonModel.setProperty("/Filters/freeSearch", '');
					oJsonModel.setProperty("/IsDraft", !!bIsDraft);

					//check if draft exist
					//בדיקה אם הגענו ממסך בקרת הזמנות
					if(!bIsDraft){
						models.CheckDraft().then((data) => {
							if(data.DraftExist){
								oComponent_PharmCr._MasterController.openDraftExsitDialog();
							}
						}).catch((error) => {
							models.handleErrors(error);
						});
					}
					

					if(!items){
						oJsonModel.setProperty("/IconTabBarKey", "RecItems");
						oJsonModel.setProperty("/allTabSelected", false);
						oJsonModel.setProperty("/genericClick", false);
						this.getItems();
					}		
				}else{
					MessageBox.error(
						oComponent_PharmCr.i18n('pilotMsg'),
						{
							onClose: function(sAction) {
								oComponent_PharmCr._MasterController.onNavBack();
							}
						}
					);
				}

			}).catch((error) => {
				models.handleErrors(error);
			});

			
		},
		_onMasterParmMatched: function(oEvent){
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				items = oJsonModel.getProperty("/items"),
				FavoriteClick =	oJsonModel.getProperty("/FavoriteClick"),
			 	bIsDraft = oEvent.getParameter("arguments").IsDraft,
				MrpArea = oEvent.getParameter("arguments").MrpArea,
				sUserName =  "";
				
				try {
					sUserName = new sap.ushell.services.UserInfo().getUser().getId();
				} catch (error) {
					sUserName = "SHIRIN"
					//console.log(error);
				}
				
			oComponent_PharmCr._fcl.byId("fcl").setLayout("OneColumn");
			oComponent_PharmCr._fcl._updateUIElements();
			
			//load abc List
			debugger;
			models.getAbcList().then((data) => {
				var jsonModel = oComponent_PharmCr.getModel("JSON"),
					aABCItems = jsonModel.getProperty("/ABCItems"),
					abcKeys = jsonModel.getProperty("/Filters/AbcKeys");
				debugger;
				if(!!data.results.length && !!data.results){
					data.results.forEach(item => {
						aABCItems.push({ value: item.AbcType, desc: item.AbcDesc });
						abcKeys.push(item.AbcType);
					});
					aABCItems.push({value:oComponent_PharmCr.i18n("EMPTY"), desc: ''});
					abcKeys.push(oComponent_PharmCr.i18n("EMPTY"));

					jsonModel.setProperty("/ABCItems", aABCItems);
					jsonModel.setProperty("/Filters/AbcKeys", abcKeys);
					jsonModel.setProperty("/AbcAllKeys", abcKeys);
					jsonModel.refresh();
				}
			}).catch((error) => {
				models.handleErrors(error);
			});
			models.getPermissionPilot(sUserName).then((data) => {			
				if(data.EvAuthorized){
					oJsonModel.setProperty("/IsDraft", !!bIsDraft);
					if(!!bIsDraft) { //ניווט ממסך בקרת הזמנות
						debugger;
						if(!!MrpArea){
						 	oJsonModel.setProperty("/Filters/Berid", MrpArea);
						}
					}

					//check if draft exist
					//בדיקה אם לא הגענו ממסך בקרת הזמנות
					if(!bIsDraft && !!bIsDraft){
						models.CheckDraft().then((data) => {
							if(data.DraftExist){
								oComponent_PharmCr._MasterController.openDraftExsitDialog();
							}
						}).catch((error) => {
							models.handleErrors(error);
						});
					}
					//if(!items || bIsDraft){ 
					if(!items){ 
						// var IconTabBarKey = oJsonModel.getProperty("/IconTabBarKey");
						// oJsonModel.setProperty("/IconTabBarKey", IconTabBarKey ? IconTabBarKey :"RecItems"); 
						oJsonModel.setProperty("/IconTabBarKey","RecItems"); 
						oJsonModel.setProperty("/allTabSelected", false);
						oJsonModel.setProperty("/genericClick", false);
						this.getItems();
					}
				}else{
						MessageBox.error(
							oComponent_PharmCr.i18n('pilotMsg'),
							{
								onClose: function(sAction) {
									oComponent_PharmCr._MasterController.onNavBack();
								}
							}
						);
				}

			}).catch((error) => {
				models.handleErrors(error);
			});
		},
		resetUiTableFilters: function (oEvent,sTableName) {		 
            var table = sTableName === 'dataTable' ? oComponent_PharmCr._MasterController.byId(sTableName) : sap.ui.getCore().byId(sTableName),
				oBinding = table.getBinding("rows"),
                columns = table.getColumns();

            for (var i = 0; i < columns.length; i++) {
                columns[i].filter(null);
				columns[i].sort(null);
				columns[i].setSorted(false);
            }
			// oBinding.sort([]);
            //oBinding.refresh(true);
        },
		onChangeBeginDate: function(oEvent){
			var value = oEvent.getSource().isValidValue(),
				oJsonModel = oComponent_PharmCr.getModel("JSON");

			if(!value){
				MessageBox.error(
					oComponent_PharmCr.i18n('DateErrMsg'),
					{
						onClose: function(sAction) {
							oJsonModel.setProperty("/Filters/BeginDateTxt",String(new Date(new Date().setDate(new Date().getDate() - 30)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear()));
							oJsonModel.setProperty("/Filters/BeginDate",new Date(new Date().setDate(new Date().getDate() - 30)));

						}
					}
				);
			}
		},
		onChangeEndDate: function(oEvent){
			var value = oEvent.getSource().isValidValue(),
				oJsonModel = oComponent_PharmCr.getModel("JSON");

			if(!value){
				MessageBox.error(
					oComponent_PharmCr.i18n('DateErrMsg'),
					{
						onClose: function(sAction) {
							oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
							oJsonModel.setProperty("/Filters/EndDate",new Date());
						}
					}
				);
			}
		},
		// onAfterRendering: function () {
        //         var sKey = 'zmm_create_pharm_or';
        //         this.oPersonalizationService = sap.ushell.Container.getService("Personalization");

        //         //const sItem = sap.ushell.services.AppConfiguration.getCurrentApplication().applicationDependencies.name;
        //         var oPersId = {
        //             container: sKey + "Personalisation", //any
        //             item: sKey //any- I have used the table name 
        //         };
        //         var oScope = {
        //             keyCategory: this.oPersonalizationService.constants.keyCategory.FIXED_KEY,
        //             writeFrequency: this.oPersonalizationService.constants.writeFrequency.LOW,
        //             clientStorageAllowed: true
        //         };

        //         // Get a Personalizer
        //         var oPersonalizer = this.oPersonalizationService.getPersonalizer(oPersId, oScope, oComponent_PharmCr);

        //         this._oTPC = new TablePersoController({
        //             table: this.getView().byId("dataTable"),
        //             componentName: sKey,
        //             persoService: oPersonalizer
        //         });
        //         // this.getColumns();
        //     },
            // getColumns: function (oEvent) {
            //     var aCols = oComponent_PharmCr._MasterController.getView().byId('dataTable').getColumns();
            //     var aColumns = [];
            //     for (var i = 1; i < aCols.length -1; i++) {
            //         if (aCols[i].getVisible()) {
            //             aColumns.push({ label: aCols[i].getLabel().getProperty("text") || " " });
            //         }
            //     }
            //     oComponent_PharmCr.getModel("JSON").setProperty("/TableColumns", aColumns);

            // },
            // onPersoButtonPressed: function (oEvent) {
            //     oComponent_PharmCr._MasterController._oTPC.openDialog();
            //     setTimeout(function () {
            //         var items = oComponent_PharmCr._MasterController._oTPC._oDialog._oList.getItems();
            //         // var aCols = oComponent_PharmCr.getModel("JSON").getProperty("/TableColumns");
            //         // for (var i = 0; i < items.length; i++) {
            //         //     var bFlag = false;
            //         //     for (var j = 0; j < aCols.length; j++) {
            //         //         if (items[i].getProperty("label") === aCols[j].label) {
            //         //             bFlag = true;
            //         //             break;
            //         //         }
            //         //     }
            //         //     if (!bFlag) {
            //                 items[0].setVisible(false);
			// 				items[10].setVisible(false);
            //     //         }

            //     //     }
            //     }, 200);
            // },
		getItems: function () {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				selectedTabKey = oJsonModel.getProperty("/IconTabBarKey"),
				allTabSelected = oJsonModel.getProperty("/allTabSelected"),
				firstGenericClick = oJsonModel.getProperty("/genericClick"),
				bIsDraft = oJsonModel.getProperty("/IsDraft"),
				countSelectedRows = oJsonModel.getProperty("/CartCount"),
				//countSelectedRows = bIsDraft ? 0 : oJsonModel.getProperty("/CartCount"),
				bIvTabsel = true; //פרמטר המחזיר את החומרים בשלושת הטאבים הראשנים

			switch (selectedTabKey) {
				case "RecItems":
				case "Favorites":
				case "ReturnBlock":
					bIvTabsel = true;
					break;
				case "AllItems":
					bIvTabsel = false;
					break;
			}
			if(firstGenericClick){ 
				bIvTabsel = false;
			}
			models.LoadItems(bIvTabsel).then((data) => {

				if (data.results.length === 0) {
					//if mrp null Branch sh will open
					this.onOpenDialog(undefined ,'BranchSh');
					// MessageToast.show(oComponent_PharmCr.i18n('noRowsMsg'), {
					// 	duration: 3000,
					// 	my: "center center",
					// 	at: "center center"
					// });
				}else {
					oJsonModel.setProperty("/Filters/Berid",data.results[0].Berid);
					oJsonModel.setProperty("/Filters/Bertx",data.results[0].Bertx);
					//אתחול כמויות הטאבים
					oJsonModel.setProperty("/RecItemsCount",data.results[0].RecomendSum);
					oJsonModel.setProperty("/FavoritesCount",data.results[0].FavoriteSum);
					oJsonModel.setProperty("/ReturnBlockCount",data.results[0].UnblockedSum); 
					oJsonModel.setProperty("/AllItemsCount",data.results[0].AllLinesSum); 
					oJsonModel.setProperty("/StockLow",data.results[0].ZstockLow); 
					oJsonModel.setProperty("/StockMedium",data.results[0].ZstockMedium); 

					oJsonModel.setProperty("/StockDaysItems",[
					{value: oComponent_PharmCr.i18n("All") , key:'N'},
					{value: oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("Less10", data.results[0].ZstockLow), key:'R'},
					{value: oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("Between10To15", [data.results[0].ZstockLow, data.results[0].ZstockMedium]) , key:'Y'},
					{value: oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("Greater15", data.results[0].ZstockMedium) , key:'G'},						
					]);

					//if table empty there is row without Matnr for the properties counts of tabs and mrp
					if(!data.results[0].Matnr){
						data.results = [];
					}

					for (var i = 0; i < data.results.length; i++) {
							//convert to filter and sort property of table
							data.results[i].IsDeleted = false; 
							data.results[i].ToltalPrice = (parseInt(Number(data.results[i].PoMenge)) * Number(data.results[i].Pharmprice)).toFixed(2);
							data.results[i].OrderQuanInt = parseInt(data.results[i].PoMenge) === 0 ? 0 : parseInt(data.results[i].PoMenge);
							data.results[i].OrderQuan = parseInt(data.results[i].PoMenge) === 0 ? '0' : parseInt(data.results[i].PoMenge).toString();
							data.results[i].OrderQuanInitial =  data.results[i].OrderQuanInt;
							data.results[i].MatnrInt = parseInt(data.results[i].Matnr);
							data.results[i].NetPriceFloat = parseFloat(data.results[i].Pharmprice);
							data.results[i].InCart = data.results[i].IsDraft;
							data.results[i].OpenQtyInt = parseFloat(parseFloat(data.results[i].OpenQty).toFixed(2));
							data.results[i].LabstInt = parseFloat(parseFloat(data.results[i].Labst).toFixed(2));
							data.results[i].Labst = parseFloat(data.results[i].Labst) ? parseFloat(data.results[i].Labst).toFixed(2) : "0";
							data.results[i].ZstockDaysInt = parseInt(data.results[i].ZstockDays);
							data.results[i].ZstockDays = parseInt(data.results[i].ZstockDays).toString();
							data.results[i].LabstLogInt = parseInt(data.results[i].LabstLog);
							data.results[i].LabstLog = parseInt(data.results[i].LabstLog).toString();
							data.results[i].IssueMenge = parseFloat(data.results[i].IssueMenge) ? parseFloat(data.results[i].IssueMenge).toFixed(2) : "0"; 
							data.results[i].IssueMengeInt = parseFloat(parseFloat(data.results[i].IssueMenge).toFixed(2));

						
							// (Number(data.results[i].IssueMenge) > 0 && data.results[i].Recomend && data.results[i].OrderQuanInt !== 0 && !data.results[i].Block) 19.11
							data.results[i].Selected = ((data.results[i].OrderQuanInt !== 0 && !data.results[i].Block && !data.results[i].Recomend) 
														||
														data.results[i].IsDraft													
													   ) ? true : false;

							//generic option
							if(data.results[i].Atcflag){
								data.results[i].itemsGen = [];
							}
							//Draft Status
							if(data.results[i].InCart){
								countSelectedRows++;
							}
						}

					}
					oJsonModel.setProperty("/CartCount", countSelectedRows);		

					if( ( (selectedTabKey === 'AllItems' && !allTabSelected) || firstGenericClick ) && !bIsDraft){ //לחיצה ראשונית על טאב הכל
						
						var aItems = oJsonModel.getProperty("/items");
						aItems= [...aItems, ...data.results];
						oJsonModel.setProperty("/allTabSelected", true);
						oJsonModel.setProperty("/items", aItems);

						if(firstGenericClick){ //לחיצה ראשונית על גנריקות
							oJsonModel.setProperty("/genericClick", false);
							oComponent_PharmCr._MasterController.getGenItems();
						}
					}else{
						oJsonModel.setProperty("/items", data.results);
						//when draft loaded all items return from backend
						if(bIsDraft){
							oJsonModel.setProperty("/allTabSelected", true);
							if(firstGenericClick){ //לחיצה ראשונית על גנריקות
								oJsonModel.setProperty("/genericClick", false);
								oComponent_PharmCr._MasterController.getGenItems();
							}
						}
					}
					oComponent_PharmCr._MasterController.filterItems();
				

			}).catch((error) => {
				models.handleErrors(error);
			});
		},
		onValueHelpSearch: function (oEvent , sType) {
			var sValue = oEvent.getParameter("value");
			var oFilter = [];
			if(sType === 'Branch'){
				 oFilter = new Filter({
					filters: [new Filter("Berid", FilterOperator.Contains, sValue),
							  new Filter("Lgobe", FilterOperator.Contains, sValue.toUpperCase())
					],
					and: false
				});
			}  
			var oBinding = oEvent.getParameter("itemsBinding");
			oBinding.filter(oFilter);
		},
		onValueHelpClose: function (oEvent, sType) {
			var oSelectedItem = oEvent.getParameter("selectedItem"),
				oJsonModel = oComponent_PharmCr.getModel("JSON");
			oEvent.getSource().getBinding("items").filter([]);
			if (!oSelectedItem) {
				this.onCloseDialog(oEvent, 'BranchSh');
				return;
			}
			else { //if user click on branch 
				var sKey = oSelectedItem.getProperty("description"),
					sDesc = oSelectedItem.getTitle();
				if (sType === 'Branch') {
					oJsonModel.setProperty("/ChangeDataAlert", oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("ChangeBranchAlert"));
					oJsonModel.setProperty("/DialogOpen", 'BranchSh');
					oJsonModel.setProperty("/BranchKey", oSelectedItem.getProperty("description"));
					oJsonModel.setProperty("/BranchDes", sDesc);

					this.onOpenDialog(oEvent, 'AlertChangeData');
				}
			}
		},
		onApproveChangeData: function (oEvent) {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				dialogNameOpen = oJsonModel.getProperty("/DialogOpen");

			this.onCloseDialog(oEvent, 'AlertChangeData');
			if(dialogNameOpen === 'BranchSh'){
				var sKey = 	oJsonModel.getProperty("/BranchKey"),
					sDesc = oJsonModel.getProperty("/BranchDes");

				oJsonModel.setProperty("/Filters/Berid", sKey);
				oJsonModel.setProperty("/Filters/Bertx", sDesc);
				oJsonModel.setProperty("/IconTabBarKey", "RecItems");
				oJsonModel.setProperty("/allTabSelected", false);
				oJsonModel.setProperty("/genericClick", false);
				// this.onCloseDialog(oEvent, 'BranchSh');

				//check if draft exist
				models.CheckDraft().then((data) => {
					if(data.DraftExist){
						oComponent_PharmCr._MasterController.openDraftExsitDialog();
					}
				}).catch((error) => {
					models.handleErrors(error);
				});
				
				this.getItems();

			}else { //dialogNameOpen === 'PeriodEdit'
				oComponent_PharmCr._MasterController.onChangePeriod(oEvent);
			}
		},
		onCancelChangeData: function (oEvent) {
			this.onCloseDialog(oEvent, 'AlertChangeData');
		},
		onClickRBPeriod: function (oEvent) {
			var selected = oEvent.getSource().getSelectedIndex(),
				oJsonModel = oComponent_PharmCr.getModel("JSON");

			switch (selected) {
				case 0:
					oJsonModel.setProperty("/Filters/BeginDate", new Date(new Date().setDate(new Date().getDate() - 7)));
					oJsonModel.setProperty("/Filters/BeginDateTxt", String(new Date(new Date().setDate(new Date().getDate() - 7)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 7)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 7)).getFullYear()));
					oJsonModel.setProperty("/Filters/EndDate",  new Date());
					oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
					break;
				case 1:
					oJsonModel.setProperty("/Filters/BeginDate", new Date(new Date().setDate(new Date().getDate() - 30)));
					oJsonModel.setProperty("/Filters/BeginDateTxt", String(new Date(new Date().setDate(new Date().getDate() - 30)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear()));
					oJsonModel.setProperty("/Filters/EndDate",  new Date());
					oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
					break;
				case 2:
					break;
			
			}
		},
		onBeforeCancelPeriod: function (oEvent) {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				LastRadioBtnPeriod = oJsonModel.getProperty("/LastRadioBtnPeriod");

				switch (LastRadioBtnPeriod) {
					case 0:
						oJsonModel.setProperty("/textPeriod",oComponent_PharmCr.i18n('LastWeek'));
						oJsonModel.setProperty("/Filters/BeginDate", new Date(new Date().setDate(new Date().getDate() - 7)));
						oJsonModel.setProperty("/Filters/BeginDateTxt", String(new Date(new Date().setDate(new Date().getDate() - 7)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 7)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 7)).getFullYear()));
						oJsonModel.setProperty("/Filters/EndDate",  new Date());
						oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
						oJsonModel.setProperty("/radioBtnPeriod",0); 

						break;
					case 1:
						oJsonModel.setProperty("/radioBtnPeriod",1); 
						oJsonModel.setProperty("/textPeriod",oComponent_PharmCr.i18n('LastMonth'));
						oJsonModel.setProperty("/Filters/BeginDate", new Date(new Date().setDate(new Date().getDate() - 30)));
						oJsonModel.setProperty("/Filters/BeginDateTxt", String(new Date(new Date().setDate(new Date().getDate() - 30)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear()));
						oJsonModel.setProperty("/Filters/EndDate",  new Date());
						oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
						break;
					case 2:
						var LastBeginDateTxt =  oJsonModel.getProperty("/LastBeginDateTxt"),
							LastEndDateTxt = oJsonModel.getProperty("/LastEndDateTxt");
							
						oJsonModel.setProperty("/radioBtnPeriod",2);

						oJsonModel.setProperty("/Filters/BeginDate", oJsonModel.getProperty("/LastBeginDate"));
						oJsonModel.setProperty("/Filters/BeginDateTxt",  LastBeginDateTxt);
						oJsonModel.setProperty("/Filters/EndDate",  oJsonModel.getProperty("/LastEndDate"));
						oJsonModel.setProperty("/Filters/EndDateTxt", LastEndDateTxt);

						oJsonModel.setProperty("/textPeriod",LastBeginDateTxt + '-' + LastEndDateTxt);
						break;
				}


			this.onCloseDialog(oEvent, 'PeriodEdit');
		},
		onBeforePressCart: function (oEvent){
			var	oJsonModel = oComponent_PharmCr.getModel("JSON"),
				aTableItems =  oJsonModel.getProperty("/items"),
				addAllSelectedInCart = true;

			for (var j = 0; j < aTableItems.length; j++) {
				if (aTableItems[j].Selected && !aTableItems[j].InCart) { //אם קיים פריט שמסומן אך לא נוסף לסל
					addAllSelectedInCart = false;
					MessageBox.alert("שים לב כי לא כל החומרים הוספו לסל, האם תרצה להוסיף אותם לסל?", {
						actions: ['לא', 'כן'],
						emphasizedAction: 'כן',
						onClose: function (sAction) {
							if (sAction === 'כן') { 
								oJsonModel.setProperty("/addAllSelectedToCart", true);
							}else{
								oJsonModel.setProperty("/addAllSelectedToCart", false);
							}
							oComponent_PharmCr._MasterController.onPressCart(oEvent);
						}
					});
					break;
				}
			}
			if(addAllSelectedInCart){
				oComponent_PharmCr._MasterController.onPressCart(oEvent);
			}

		},
		onPressCart: function (oEvent) {
			var	oJsonModel = oComponent_PharmCr.getModel("JSON"),
				aTableItems =  oJsonModel.getProperty("/items"),
				CartCount = oJsonModel.getProperty("/CartCount"),
				addAllSelectedToCart = oJsonModel.getProperty("/addAllSelectedToCart"),
				existMatnrNotValid = false,
				TotalPrice = 0,
				countSelectedRows = 0,
				aCartItems = [];

			if(!CartCount && !addAllSelectedToCart){
				MessageToast.show(oComponent_PharmCr.i18n('noExistItemsMsg'), {
					duration: 3000
				});
				return;
			}
			if(addAllSelectedToCart){ //לחיצה על הוספת חומרים לסל בעת לחיצה על ביצוע הזמנה

				for (var i = 0; i < aTableItems.length; i++) {
					if (aTableItems[i].Selected && !aTableItems[i].InCart) {
						let valid = this.checkOrderQuan(aTableItems[i], true);
						if(valid){
							oJsonModel.setProperty("/items/" + i + '/InCart', true);
						}else{
							existMatnrNotValid = true;
						}
					}
				}
				if(existMatnrNotValid){
					oJsonModel.setProperty("/addToCartClicked", true);
				}
				oJsonModel.setProperty("/addAllSelectedToCart", false);

			}
			
			for (var i = 0; i < aTableItems.length; i++) {
				if (aTableItems[i].Selected && aTableItems[i].InCart) {
						aCartItems.push(aTableItems[i]);
						TotalPrice += aTableItems[i].OrderQuanInt * aTableItems[i].NetPriceFloat;
						countSelectedRows++;
				}
			}

			oJsonModel.setProperty("/CartCount", !!countSelectedRows ? countSelectedRows : CartCount);
			oJsonModel.setProperty("/TotalPrice", TotalPrice.toFixed(2));
			oJsonModel.setProperty("/CartItems", jQuery.extend(true, [], aCartItems));
			oJsonModel.setProperty("/oldCartItems", jQuery.extend(true, [], aCartItems));
			oJsonModel.setProperty("/oldCartTotalPrice", TotalPrice.toFixed(2));
			oJsonModel.setProperty("/cartTotalPrice", TotalPrice.toFixed(2));
			
			if(!existMatnrNotValid) //הסל יפתח במקרה של לחיצה על ביצוע הזמנה והוספת פריטים שסומנו לסל רק במידה והכמויות תקינות
				this.onOpenDialog('', 'Cart');
			
		},
		//Function when click on addCart button 
		onPressAddCart: function (oEvent) {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				aTableItems = oJsonModel.getProperty("/items"),
				TotalPrice = 0,
				countSelectedRows = 0,
				aCartItems = [],
				existSeleced = false;
			
			oJsonModel.setProperty("/addToCartClicked", true);

			for (var i = 0; i < aTableItems.length; i++) {
				if (aTableItems[i].Selected) {
					existSeleced = true;
					let valid = this.checkOrderQuan(aTableItems[i], true);

					if(valid){
						oJsonModel.setProperty("/items/" + i + '/InCart', true);
						countSelectedRows++;
					}
					// else{
					// 	continue;
					// }
				}
			}

			if (!existSeleced) {
					MessageToast.show(oComponent_PharmCr.i18n('noSelectedItemsMsg'), {
						duration: 3000
				});
				return;
			}else{
		 		oJsonModel.setProperty("/CartCount", countSelectedRows);		
			}
			
		},
		onPressGenerics: function (oEvent) {
			var oRow = oEvent.getSource().getBindingContext("JSON").getObject(),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				allTabSelected = oJsonModel.getProperty("/allTabSelected"),
				genericClick = oJsonModel.getProperty("/genericClick"),
				sPath = oEvent.getSource().getBindingContext("JSON").getPath();
			
			oJsonModel.setProperty("/oRowGen",oRow);
			oJsonModel.setProperty("/sPathGen",sPath);

			if(!genericClick){ // the user don't click on genButton and this is the first time
				oJsonModel.setProperty("/genericClick",true);
			}

			//if allItems materials don't uploaded ,טעינת כל החומרים במידה ולחצו על גנריקות וכל החומרים לא נשלפו עדיין כדי שיהיה ניתן להוסיפם לסל
			if(!allTabSelected){ 
				this.getItems();
			}else{
				this.getGenItems();
			}
		},
		getGenItems: function(){
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				itemsMaster =  oJsonModel.getProperty("/items"),
				oRow = oJsonModel.getProperty("/oRowGen"),
				sPath = oJsonModel.getProperty("/sPathGen"),
				aItemsGenerics = JSON.parse(JSON.stringify(oRow['itemsGen']));
				
			if(!aItemsGenerics.length){
				models.LoadItemsGen(oRow.Atccode,oRow.Matnr).then((data) => {
					var aData = data.results;
					//aData = aData.filter(item => item.Matnr !== matnrNum);
					for (var i = 0; i < aData.length; i++) {
						//convert to filter and sort property of table
						data.results[i].IsDeleted = false; 
						aData[i].ToltalPrice = (parseInt(Number(aData[i].PoMenge)) * Number(aData[i].Pharmprice)).toFixed(2);
						aData[i].OrderQuanInt = parseInt(aData[i].PoMenge) === 0 ? 0 : parseInt(aData[i].PoMenge);
						aData[i].OrderQuan = parseInt(aData[i].PoMenge) === 0 ? '0' : parseInt(aData[i].PoMenge).toString();
						aData[i].OrderQuanInitial =  aData[i].OrderQuanInt;
						aData[i].MatnrInt = parseInt(aData[i].Matnr);
						aData[i].NetPriceFloat = parseFloat(aData[i].Pharmprice);
						aData[i].InCart = false;
						aData[i].OpenQtyInt = parseInt(aData[i].OpenQty); 
						aData[i].LabstInt = parseFloat(parseFloat(aData[i].Labst).toFixed(2));
						aData[i].Labst = parseFloat(aData[i].Labst).toFixed(2) ? parseFloat(aData[i].Labst).toFixed(2) : "0";
						aData[i].ZstockDaysInt = parseInt(aData[i].ZstockDays);
						aData[i].ZstockDays = parseInt(aData[i].ZstockDays).toString();
						aData[i].LabstLogInt = parseInt(aData[i].LabstLog);
						aData[i].LabstLog = parseInt(aData[i].LabstLog).toString();
						aData[i].IssueMenge = parseFloat(aData[i].IssueMenge).toFixed(2) ? parseFloat(aData[i].IssueMenge).toFixed(2) : "0";
						aData[i].IssueMengeInt =  parseFloat(parseFloat(aData[i].IssueMenge).toFixed(2));
						aData[i].Selected = aData[i].OrderQuanInt !== 0  && !aData[i].Block ? true : false;
					}
					aItemsGenerics = aData;
					oComponent_PharmCr._MasterController.setCurrItemsGen(itemsMaster,aItemsGenerics,oRow,sPath);
				}).catch((error) => {
					models.handleErrors(error);
				});
			}else{
				oComponent_PharmCr._MasterController.setCurrItemsGen(itemsMaster,aItemsGenerics,oRow,sPath);
			}

		},
		setCurrItemsGen: function(itemsMaster,aItemsGenerics,oRow,sPath){
			var model = oComponent_PharmCr.getModel("JSON");
			//update aItemsGenerics list according to items
			for (var i = 0; i < aItemsGenerics.length; i++) {
				for(var j = 0; j < itemsMaster.length; j++){
					if(aItemsGenerics[i].Matnr === itemsMaster[j].Matnr){
						aItemsGenerics[i].Selected = itemsMaster[j].Selected;
						aItemsGenerics[i].OrderQuan = itemsMaster[j].OrderQuan;
						aItemsGenerics[i].OrderQuanInitial = itemsMaster[j].OrderQuanInitial;
						aItemsGenerics[i].OrderQuanInt = itemsMaster[j].OrderQuanInt;
						aItemsGenerics[i].InCart = itemsMaster[j].InCart;
						aItemsGenerics[i].ToltalPrice = itemsMaster[j].ToltalPrice;
						break;

					}
				}
			}
			model.setProperty("/currItemsGen",JSON.parse(JSON.stringify(aItemsGenerics)));
			model.setProperty("/SelectedRowToGeneric", oRow);
			model.setProperty("/SelectedPathToGeneric", sPath);
			model.refresh(true);

			this.onOpenDialog('', 'Generics');		
			//oComponent_PharmCr._MasterController.overrideUiTableFilter("genericTable");
			oComponent_PharmCr._MasterController.filterItemsGen(0);

		},
		onGenDialogAfterOpen: function(oEvent){
			oComponent_PharmCr._MasterController.overrideUiTableFilter("genericTable");
		},
		onBeforeOpenPeriodDialog: function(oEvent){
			var model = oComponent_PharmCr.getModel("JSON");
			
			model.setProperty("/LastRadioBtnPeriod",model.getProperty("/radioBtnPeriod"));
			model.setProperty("/LastBeginDateTxt",model.getProperty("/Filters/BeginDateTxt"));
			model.setProperty("/LastBeginDate",model.getProperty("/Filters/BeginDate"));
			model.setProperty("/LastEndDateTxt",model.getProperty("/Filters/EndDateTxt"));
			model.setProperty("/LastEndDate",model.getProperty("/Filters/EndDate"));

			this.onOpenDialog(oEvent, 'PeriodEdit');		

		},
		onClickRBGen:function (oEvent) {	
			var key = oEvent.getSource().getSelectedIndex(),
				jsonModel = oComponent_PharmCr.getModel("JSON");

			oComponent_PharmCr._MasterController.filterItemsGen(key);
		},
		onApproveGenerics: function (oEvent) {	
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				acurrItemsGen = oJsonModel.getProperty("/currItemsGen"),
				aMasterItems = oJsonModel.getProperty("/items"),
				iCartCount = oJsonModel.getProperty("/CartCount") ? oJsonModel.getProperty("/CartCount") : 0,
				rowTable = oJsonModel.getProperty("/SelectedRowToGeneric"),
				pathRowTable = oJsonModel.getProperty("/SelectedPathToGeneric"),
				aGenItemsOfRow = oJsonModel.getProperty(pathRowTable + "/itemsGen"),
				valid = true,
				countSelectedRows = 0,
				bAddToCart = false,
				noSelectedItems = false;
				
				oJsonModel.setProperty("/approveGenClicked", true); 

			if(acurrItemsGen.length > 0){
				for (var i = 0; i < acurrItemsGen.length; i++) {
					if(acurrItemsGen[i].Selected){
						noSelectedItems = true;
						valid = this.checkOrderQuan(acurrItemsGen[i],true);
						if(!valid)
							return;
						acurrItemsGen[i].InCart = true;

					}	
				}
			
				if(!noSelectedItems){
					MessageToast.show(oComponent_PharmCr.i18n('noSelectedItemsMsg'), {
						duration: 3000
					});
					return;
				}
			

			oJsonModel.setProperty( pathRowTable + "/itemsGen",JSON.parse(JSON.stringify(acurrItemsGen)));
			oJsonModel.setProperty( pathRowTable + "/Selected",false);

			for (var i = 0; i < acurrItemsGen.length; i++) {
				for (var j = 0; j < aMasterItems.length; j++) {

					if(acurrItemsGen[i].Matnr === aMasterItems[j].Matnr && acurrItemsGen[i].Selected){
						bAddToCart = true;
						aMasterItems[j].Selected = true;
						aMasterItems[j].OrderQuan = acurrItemsGen[i].OrderQuan;
						aMasterItems[j].OrderQuanInitial = acurrItemsGen[i].OrderQuanInitial;
						aMasterItems[j].OrderQuanInt = acurrItemsGen[i].OrderQuanInt;
						aMasterItems[j].InCart = acurrItemsGen[i].InCart;
						aMasterItems[j].ToltalPrice = acurrItemsGen[i].ToltalPrice;
						break;

					}else if(acurrItemsGen[i].Matnr === aMasterItems[j].Matnr){
						aMasterItems[j].Selected = false;
						break;
					}
				}
			}
			//reset filter in generic dialog
			oComponent_PharmCr._MasterController.filterItemsGen(0);
			oJsonModel.setProperty("/radioBtnAtc",0);

			//update countSelectedRows
			for (var i = 0; i < aMasterItems.length; i++) {
				if (aMasterItems[i].Selected && aMasterItems[i].InCart) {
						countSelectedRows++;
				}
			}
		 	oJsonModel.setProperty("/CartCount", countSelectedRows);		
		}
	
		this.onCloseDialog('', 'Generics');
		if(bAddToCart){
			MessageToast.show(oComponent_PharmCr.i18n('genericAddCart'), {
				duration: 2000
			});
		}
		oJsonModel.setProperty("/approveGenClicked", false); 

		},
		onChangeOrderCount: function (oEvent) {
			var value = oEvent.getSource().getValue(),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				sPath = oEvent.getSource().getBindingContext("JSON").getPath(),
				valid = true,
				isSelected = false,
				item = oJsonModel.getProperty(sPath);

				item.OrderQuan = value;
				item.OrderQuanInt = +value;

				valid = this.checkOrderQuan(item, false),
				isSelected =  valid;

			oEvent.getSource().getBindingContext("JSON").getObject().Selected = isSelected;

		},
		onEnterOrderCount: function (oEvent) {
			var sValue = oEvent.getSource().getValue(),
				onlyNumVal = sValue.replace(/[^\.\d]/g, ''),			//change input only number without signs and letters
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				sPath = oEvent.getSource().getBindingContext("JSON").getPath(),
				maxQuan = Number(oJsonModel.getProperty(sPath + '/Maxqut')),
				roundCount = Number(oJsonModel.getProperty(sPath + '/Bstrf'));

			if(onlyNumVal.indexOf(".") !== -1 && onlyNumVal.split(".")[1].length > 3){
				onlyNumVal = parseFloat(onlyNumVal).toFixed(3);
			}
			oEvent.getSource().getBindingContext("JSON").getObject().Selected =  false;
			oJsonModel.setProperty(sPath + '/OrderQuanInt', Number(onlyNumVal));
			oJsonModel.setProperty(sPath + '/OrderQuan', onlyNumVal);
			oEvent.getSource().setValue(onlyNumVal);
		
			if( Number(onlyNumVal) > maxQuan && maxQuan){
				MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("MaxCount", maxQuan), {
					duration: 2000
				});
				oJsonModel.setProperty(sPath + '/errorValue', true);
				return;
			// }else if( Number(onlyNumVal) % roundCount !== 0 && roundCount){
			}else if( !!(Number(onlyNumVal) / roundCount).toString().split(".")[1] && roundCount){
				MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("RoundCount", roundCount), {
					duration: 2000
				});
				oJsonModel.setProperty(sPath + '/errorValue', true);
				return;
			}
			if(Number(onlyNumVal)){
				oJsonModel.setProperty(sPath + '/Selected', true);
				oJsonModel.setProperty(sPath + '/errorValue', false);
			}
		}, 
		onEnterOrderCartCount: function (oEvent) {
			var sValue = oEvent.getSource().getValue(),
				onlyNumVal = sValue.replace(/[^\.\d]/g, ''),			//change input only number without signs and letters
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				sPath = oEvent.getSource().getBindingContext("JSON").getPath(),
				maxQuan = Number(oJsonModel.getProperty(sPath + '/Maxqut')),
				roundCount = Number(oJsonModel.getProperty(sPath + '/Bstrf')),
				oRow = oEvent.getSource().getBindingContext("JSON").getObject(),
				TotalPrice = 0;

				if(onlyNumVal.indexOf(".") !== -1 && onlyNumVal.split(".")[1].length > 3){
					onlyNumVal = parseFloat(onlyNumVal).toFixed(3);
				}
				
				oEvent.getSource().setValue(onlyNumVal);
				oJsonModel.setProperty(sPath + '/TotalPrice', Number(onlyNumVal) * Number(oRow.NetPriceFloat));

				var items = oComponent_PharmCr.getModel("JSON").getProperty("/CartItems");
				for (var i = 0; i < items.length; i++) {
					TotalPrice += +(items[i].OrderQuan) * +(items[i].NetPriceFloat);
				}
				oComponent_PharmCr.getModel("JSON").setProperty("/cartTotalPrice", TotalPrice.toFixed(2));
				

			oJsonModel.setProperty(sPath + '/OrderQuanInt', Number(onlyNumVal));
			oJsonModel.setProperty(sPath + '/OrderQuan', onlyNumVal);
			oJsonModel.setProperty(sPath + '/errorValue', false);
			
			if( Number(onlyNumVal) > maxQuan && maxQuan){
				MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("MaxCount", maxQuan), {
					duration: 2000
				});
				oJsonModel.setProperty(sPath + '/errorValue', true);
				return;
			}else if( !!(Number(onlyNumVal) / roundCount).toString().split(".")[1] && roundCount){
				MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("RoundCount", roundCount), {
					duration: 2000
				});
				oJsonModel.setProperty(sPath + '/errorValue', true);
				return;
			}
		}, 
		onMultiTokenLiveChange: function (oEvent) {
			const sValues = oEvent.getParameter("newValue");
			const aValues = sValues.split(" ");
			const oMultiInput = oEvent.getSource();

			if (aValues.length > 1) {
				for (let i in aValues) {
					if (!!aValues[i]) {
						let oToken = new sap.m.Token({
							key: aValues[i],
							text: aValues[i],
						});
						oMultiInput.addToken(oToken);
					}
				}
				setTimeout(() => {
					oMultiInput.setValue("");
				}, 0);
				oMultiInput.fireChange(); //because in copy paste its not work
			}
		},
		onMultiTokenChange: function (oEvent) {
				const sValue = oEvent.getParameter("newValue");
				const oMultiInput = oEvent.getSource();
				if (!!sValue) {
					let oToken = new sap.m.Token({
						key: sValue,
						text: sValue,
					});
					oMultiInput.addToken(oToken);
					setTimeout(() => {
						oMultiInput.setValue("");
					}, 0);
				}
				var tokens = oEvent.getSource().getTokens(),
					aMatnrNumbers = [];

				oComponent_PharmCr.getModel("JSON").setProperty("/deleteMultiMatrialVisible", !!tokens.length);

				if(tokens.length){
					for(var i=0; i < tokens.length; i++){
						aMatnrNumbers.push(tokens[i].getProperty("key"));
					}
				}

				this.filterItems(oEvent,aMatnrNumbers);

				
		},
		onUpdateToken: function(oEvent){
			//when delete number or numbers
			var oMultiInput = oEvent.getSource(),
			 	aRemoveTokens = oEvent.getParameter("removedTokens"),
				fromDeleteBtn = false;
			
			//when user click from delete btn
			if(!aRemoveTokens){
				aRemoveTokens = oComponent_PharmCr._MasterController.getView().byId("MatnrMulti").getTokens();
				oMultiInput = oComponent_PharmCr._MasterController.getView().byId("MatnrMulti");
				fromDeleteBtn = true;

			}
			for(let i in aRemoveTokens){
				oMultiInput.removeToken(aRemoveTokens[i]);
			}
			var aValues = fromDeleteBtn ? oComponent_PharmCr._MasterController.getView().byId("MatnrMulti").getTokens() : oEvent.getSource().getTokens();
			oComponent_PharmCr.getModel("JSON").setProperty("/deleteMultiMatrialVisible", !!aValues.length);
			oMultiInput.fireChange();
		},
		onDeleteAllABCFilter: function(oEvent){
			debugger;
			var modelJson = oComponent_PharmCr.getModel("JSON");
			modelJson.setProperty("/deleteMultiABCVisible", false);
			modelJson.setProperty("/Filters/AbcKeys",[]);
			modelJson.refresh();
			oComponent_PharmCr._MasterController.filterItems();
		},
		onExportToExcel: function (oEvent, isFromCart) {
			var aCols, aItems, oSettings, oSheet;
			aItems = oComponent_PharmCr.getModel("JSON");
			var aSelectedModel = [];

			if(isFromCart){
				var aIndices = sap.ui.getCore().byId("dataTableCart").getBinding("rows").aIndices,
					sItems = "/CartItems/";
				aCols = this.createCartColumnConfig();
			}else{
				var aIndices = oComponent_PharmCr._MasterController.byId("dataTable").getBinding("rows").aIndices,
					sItems = "/items/";
				aCols = this.createColumnConfig();
			}

			var aSelectedModel = [];
			for (var i = 0; i < aIndices.length; i++) {
				aSelectedModel.push(aItems.getProperty(sItems + aIndices[i]));
			}
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedModel,
				fileName: "הזמנת תרופות וציוד רפואי לבית מרקחת"
			};

			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function () { });

		},
		createColumnConfig: function () {
			var aCols = oComponent_PharmCr._MasterController.byId("dataTable").getColumns();
			var aColumns = [];
			// aColumns.push({
			// 	label: oComponent_PharmCr.i18n('isSelectMatnr'),
			// 	property: 'Selected'
			// });
			aColumns.push({
				label: oComponent_PharmCr.i18n('ATC'),
				property: 'Atccode'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('MaterialType'),
				property: ['Mtart','Maabc'],
				template: '{0} {1}'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('MaterialNum'),
				property: 'Matnr'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('statusMatrial'),
				property: 'Mstae'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('MaterialDesc'),
				property: 'Maktx'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('AmountOrder'),
				property: 'OrderQuanInt'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('StockBimk') + '(' + oComponent_PharmCr.i18n('TransStock') + ')',
				property: ['OpenQtyInt', 'LabstInt'],
				template: '({0}) {1}'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('StockDays'),
				property: 'ZstockDays'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('StockMarlog'),
				property: 'LabstLogInt'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('Issued'),
				property: 'IssueMenge'
			});
			
			return aColumns;
		},
		// onExportCartToExcel: function (oEvent) {
		// 	var aCols, aItems, oSettings, oSheet;
		// 	aCols = this.createCartColumnConfig();
		// 	aItems = oComponent_PharmCr.getModel("JSON");
		// 	var aSelectedModel = [];
		// 	var aSelectedModel = [];
		// 	var aIndices = oComponent_PharmCr._MasterController.byId("dataTable").getBinding("rows").aIndices;
		// 	for (var i = 0; i < aIndices.length; i++) {
		// 		aSelectedModel.push(aItems.getProperty("/CartItems/" + aIndices[i]));
		// 	}
		// 	oSettings = {
		// 		workbook: {
		// 			columns: aCols
		// 		},
		// 		dataSource: aSelectedModel,
		// 		fileName: "הזמנת תרופות וציוד רפואי לבית מרקחת"
		// 	};

		// 	oSheet = new Spreadsheet(oSettings);
		// 	oSheet.build()
		// 		.then(function () { });

		// },
		createCartColumnConfig: function () {
			var aCols = sap.ui.getCore().byId("dataTableCart").getColumns();
			var aColumns = [];
			aColumns.push({
				label: oComponent_PharmCr.i18n('ATC'),
				property: 'Atccode'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('MaterialNum'),
				property: 'Matnr'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('MaterialDesc'),
				property: 'Maktx',
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('AmountOrder'),
				property: 'OrderQuanInt'
			});
			aColumns.push({
				label: oComponent_PharmCr.i18n('yarpaPrice'),
				property: ['Waers','Pharmprice'],
				template: '{0} {1}'
			});
			// aColumns.push({
			// 	label: oComponent_PharmCr.i18n('ParmComment'),
			// 	property: 'PrComment'
			// });			
			return aColumns;
		},
		onListItemPress: function (oEvent) {
			// var selectRowIndex = oEvent.getSource().getSelectedIndex(),
			// 	oJsonModel = oComponent_PharmCr.getModel("JSON"),
			// 	oRow =  oComponent_PharmCr.getModel("JSON").getProperty("/items/" + selectRowIndex);
			if(!!oEvent.getSource().getBindingContext("JSON").getObject()){
				
				var oRow = oEvent.getSource().getBindingContext("JSON").getObject();
				if(!!oRow){
					var sMaterial = oRow.Matnr ? oRow.Matnr : "",
					sMrpArea = oRow.Berid,
					sFromDate = oRow.FromDate.toISOString().split('T')[0],
					sToDate = oRow.ToDate.toISOString().split('T')[0],
					sCurrTab = oComponent_PharmCr.getModel("JSON").getProperty("/Routes/SelectedTab"),
					navObj = {
						 Material: sMaterial,
						 MrpArea: sMrpArea,
						 FromDate: sFromDate,
						 ToDate: sToDate,
						 tab: sCurrTab
					};
					oComponent_PharmCr.getModel("JSON").setProperty("/SelectedItem", oRow);	
					this.getDataOfTabs(sCurrTab, sMaterial, sMrpArea, sFromDate, sToDate);
					this.oRouter.navTo("detail", navObj);
				}
			}
		},
		onBeforeChangePeriod: function (oEvent) {
				var oJsonModel = oComponent_PharmCr.getModel("JSON");
				oJsonModel.setProperty("/ChangeDataAlert", oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("ChangePeriodAlert"));
				oJsonModel.setProperty("/DialogOpen", 'PeriodEdit');

				this.onOpenDialog(oEvent, 'AlertChangeData');
		},
		onChangePeriod: function (oEvent) {
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				BeginDateTxt = oJsonModel.getProperty("/Filters/BeginDateTxt"),
				BeginDate = oJsonModel.getProperty("/Filters/BeginDate"),
				EndDateTxt = oJsonModel.getProperty("/Filters/EndDateTxt"),
				EndDate = oJsonModel.getProperty("/Filters/EndDate"),
				sPeriodKey = oJsonModel.getProperty("/radioBtnPeriod");
				
				switch (sPeriodKey) {
					case 0:
						oJsonModel.setProperty("/textPeriod",oComponent_PharmCr.i18n('LastWeek'));
						oJsonModel.setProperty("/Filters/BeginDate", new Date(new Date().setDate(new Date().getDate() - 7)));
						oJsonModel.setProperty("/Filters/BeginDateTxt", String(new Date(new Date().setDate(new Date().getDate() - 7)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 7)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 7)).getFullYear()));
						oJsonModel.setProperty("/Filters/EndDate",  new Date());
						oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
						break;
					case 1:
						oJsonModel.setProperty("/textPeriod",oComponent_PharmCr.i18n('LastMonth'));
						oJsonModel.setProperty("/Filters/BeginDate", new Date(new Date().setDate(new Date().getDate() - 30)));
						oJsonModel.setProperty("/Filters/BeginDateTxt", String(new Date(new Date().setDate(new Date().getDate() - 30)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear()));
						oJsonModel.setProperty("/Filters/EndDate",  new Date());
						oJsonModel.setProperty("/Filters/EndDateTxt", String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()));
						break;
					case 2:
						oJsonModel.setProperty("/textPeriod",BeginDateTxt + '-' + EndDateTxt);

						if(BeginDate > EndDate || EndDate > new Date() || BeginDate > new Date()){
							MessageToast.show(oComponent_PharmCr.i18n('SearchPageDatesErrorMsg'),{
								duration: 4000});
							return;
						}
						break;
				}
				this.onCloseDialog(oEvent, 'PeriodEdit');
				oJsonModel.setProperty("/IconTabBarKey", "RecItems");
				oJsonModel.setProperty("/allTabSelected", false);
				oJsonModel.setProperty("/genericClick", false);
				oJsonModel.setProperty("/CartCount", 0);
				
				this.getItems();

		},
		onCloseGenDialog: function (oEvent) {
			oComponent_PharmCr.getModel("JSON").setProperty("/radioBtnAtc",0);
			this.filterItemsGen(0);
			this.onCloseDialog(oEvent, 'Generics');

		},
		clearFilters: function (oEvent) {
			var model = oComponent_PharmCr.getModel("JSON"),
				oJsonModel = model.getData(),
				abcAllKeys = oJsonModel.AbcAllKeys;
		
			model.setProperty("/Filters/ShowBlockMat", true);
			model.setProperty("/Filters/ShowRecNupko", false);
			model.setProperty("/Filters/MateiralType", '1');
			model.setProperty("/Filters/CoolKey", '1');
			model.setProperty("/Filters/DrugsKey", '1');
			model.setProperty("/Filters/AbcKeys", abcAllKeys);
			model.setProperty("/Filters/freeSearch", '');
			model.setProperty("/Filters/StockDayKey", 'N');

			oComponent_PharmCr._MasterController.filterItems();
		},
		filterItemsAndTab: function (oEvent){
			var selectedTab = oEvent.getSource().getSelectedKey(),
				model = oComponent_PharmCr.getModel("JSON"),
				allTabSelected = model.getProperty("/allTabSelected"),
				bIsDraft = model.getProperty("/IsDraft");
				
				if(!allTabSelected && selectedTab === 'AllItems' && !bIsDraft){ //שליפה ראשונה של כלל החומרים
					this.getItems(); 
				}else{
					this.filterItems(oEvent);
				}

		},
		filterItems: function (oEvent, aMatnrNumbers) {
			var aFilters = [],
			 	model = oComponent_PharmCr.getModel("JSON"),
				fData = model.getProperty("/Filters"),
				selectedTab =  model.getProperty("/IconTabBarKey");

			//	פילטר טאב המלצות
			switch (selectedTab) {
				case 'RecItems':
					aFilters.push(new Filter("Recomend", "EQ", true));
					break;
			
				case 'Favorites':
					aFilters.push(new Filter("Favorite", "EQ", true));
					break;

				case 'ReturnBlock':
					aFilters.push(new Filter("Unblocked", "EQ", true));
					break;

			}
			//פילטר הצגת חומרים חסומים
			if (!fData.ShowBlockMat) {
				aFilters.push(new Filter("Block", "EQ", fData.ShowBlockMat));
			}
			// פילטר הצגת חומרים ממולצים שנופקו וגם חומרים שהנופקו גדול מ0 בטאב הכל
			if (fData.ShowRecNupko && selectedTab !== 'Favorites' &&  selectedTab !== 'ReturnBlock' ) {
				aFilters.push(new Filter("IssueMenge",FilterOperator.GT, '0'));
			}
			//פילטר סוג חומר
			if (fData.MateiralType !== '1') {
				var sMtart = fData.MateiralType === '2' ? 'ZPRP' : 'ZSPN';
				aFilters.push(new Filter("Mtart", "EQ", sMtart));
			}
			//פילטר סמים
			if (fData.DrugsKey !== '1') {
				var bDrugs = fData.DrugsKey === '2' ? true : false;
				aFilters.push(new Filter("Drugs", "EQ", bDrugs));
			}
			//פילטר עבור קירור
			if (fData.CoolKey !== '1') {
				if(fData.CoolKey === '4'){
					aFilters.push(new Filter({
						filters: [ new Filter("Raube",FilterOperator.NE,'11'),
								   new Filter("Raube",FilterOperator.NE,'1')
						],
						and: true
					}));

				}else{
					var sCool = fData.CoolKey === '2' ? '1' : '11' ;
					aFilters.push(new Filter("Raube", "EQ", sCool));
				}
				
			}
			//פילטר ימי מלאי
			if (fData.StockDayKey !== 'N') {				
				aFilters.push(new Filter("StockColor", "EQ", fData.StockDayKey));
			}
			//חיפוש
			if (fData.freeSearch && fData.freeSearch.length > 0) {
				aFilters.push(new Filter({
					filters: [ new Filter('Maktx', FilterOperator.Contains, fData.freeSearch),
							   new Filter('Matnr', FilterOperator.Contains, fData.freeSearch),
							   new Filter('Atccode', FilterOperator.Contains, fData.freeSearch),
					],
					and: false
				}));
			}
			if(!!aMatnrNumbers && aMatnrNumbers.length){
				var matNumFilter = new Filter({
					filters: [],
					and: false
				});
				for(var i=0; i< aMatnrNumbers.length; i++){
					matNumFilter.aFilters.push(new Filter('Matnr',"EQ", aMatnrNumbers[i]));
				}
				aFilters.push(matNumFilter);


			}
			//חיפוש לפי ABC 
			var aAbcLen = fData.AbcKeys.length,
				aAbc = fData.AbcKeys;
			debugger;
			if(aAbcLen){
				// model.setProperty("/Filters/AbcKeys", aAbc);
				// model.refresh();

				model.setProperty("/deleteMultiABCVisible", true);
				var abcFilter = new Filter({
					filters: [],
					and: false
				});
				for(var i = 0; i < aAbcLen; i++){
					if(aAbc[i] === oComponent_PharmCr.i18n("EMPTY")){
						abcFilter.aFilters.push(new Filter('Maabc', "EQ", ''));
					}else{
						abcFilter.aFilters.push(new Filter('Maabc', "EQ", aAbc[i]));
					}
				}
				aFilters.push(abcFilter);
			}else{ //אם פילטר ABC ריק כלומר המשתמש לא רוצה אף אחת מהאופציות
				debugger;
				model.setProperty("/deleteMultiABCVisible", false);
				var abcFilter = new Filter({
					filters: [],
					and: true
				});
				debugger;
				var aAbcAllKeys = model.getProperty("/AbcAllKeys");
				aAbcAllKeys.forEach(item => {
					if(item === oComponent_PharmCr.i18n("EMPTY")){
						abcFilter.aFilters.push(new Filter('Maabc', "NE", ''));
					}else{
						abcFilter.aFilters.push(new Filter('Maabc', "NE", item));
					}
				});
				aFilters.push(abcFilter);

			}
			var oTable = oComponent_PharmCr._MasterController.getView().byId("dataTable");
			var oBinding = oTable.getBinding("rows");
			oBinding.filter(aFilters, "Application");
			oComponent_PharmCr.getModel("JSON").setProperty("/" + selectedTab + "Count", oBinding.getLength());
			//var count = oTable.getBinding("rows").getLength();

		},
		filterItemsGen: function (keyFilterAtc) {
			var aFilters = [],
			 	model = oComponent_PharmCr.getModel("JSON");

				 switch (keyFilterAtc) {
					case 0:
						aFilters.push(new Filter("IsAtc11", "EQ", true));
						break;
					case 1:
						aFilters.push(new Filter("IsAtc9", "EQ", true));
						break;
					case 2:
						aFilters.push(new Filter("IsAtc7", "EQ", true));
						break;
				}			
				
			
			var oTable = sap.ui.getCore().byId("genericTable");
			var oBinding = oTable.getBinding("rows");
			oBinding.filter(aFilters);

		}
	
	});
});

