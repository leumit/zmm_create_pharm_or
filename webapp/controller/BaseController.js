sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/ui/core/UIComponent",
        "zmm_create_pharm_or/model/formatter",
        "zmm_create_pharm_or/model/models",
        "sap/ui/model/Filter",
        "sap/ui/model/Sorter",
        "sap/ui/model/FilterOperator",
        "sap/m/MessageBox",
        'sap/m/MessageToast',
	"sap/ui/model/odata/type/Decimal"
        ],
    function (Controller, History, UIComponent, formatter, models, Filter, Sorter, FilterOperator, MessageBox, MessageToast) {
        "use strict";
        return Controller.extend("zmm_create_pharm_or.controller.BaseController", {
            formatter: formatter,

            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel: function (sName) {
                return this.getView().getModel(sName);
            },

            /**
             * Convenience method for setting the view model in every controller of the application.
             * @public
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} sName the model name
             * @returns {sap.ui.core.mvc.View} the view instance
             */
            setModel: function (oModel, sName) {
                return this.getView().setModel(oModel, sName);
            },

            /**
             * Convenience method for getting the resource bundle.
             * @public
             * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
             */
            getResourceBundle: function () {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle();
            },

            /**
             * Method for navigation to specific view
             * @public
             * @param {string} psTarget Parameter containing the string for the target navigation
             * @param {Object.<string, string>} pmParameters? Parameters for navigation
             * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
             */
            navTo: function (psTarget, pmParameters, pbReplace) {
                this.getRouter().navTo(psTarget, pmParameters, pbReplace);
            },

            getRouter: function () {
                return UIComponent.getRouterFor(this);
            },
            onOpenDialog: function (oEvent, sDialogName) {

                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("zmm_create_pharm_or.view.popovers." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();

                if(sDialogName === 'Cart'){
                    let aItems = oComponent_PharmCr.getModel("JSON").getProperty("/items");
                    oComponent_PharmCr._MasterController.checkValidMatnr(aItems);
                }

            },
            onCloseDialog: function (oEvent, sDialogName) {
                try {
                    this[sDialogName].close();
                } catch (e) {
                    oComponent_PharmCr[sDialogName].close();
                }
            },
            onBackCartDialog: function (oEvent){
                //reset sort and filter table
                let table = sap.ui.getCore().byId("dataTableCart"),
                    oListBinding = table.getBinding(),
                    columns = table.getColumns();

                for (var i = 0; i < columns.length; i++) {
                    columns[i].setFilterValue("");
                    columns[i].setFiltered();
                    columns[i].setSorted(false);
                }
                table.getBinding("rows").filter();

                oListBinding.aSorters = null;
                oListBinding.aFilters = null;
                oListBinding.refresh(true);
                oComponent_PharmCr.getModel("JSON").refresh(true);

                this.onCloseDialog('', 'Cart');
            },
            onOpenPopover: function (oEvent, sName, sTitle, sBody, sFrom) {
                var oRow = {};
                if (!this[sName]) {
                    this[sName] = sap.ui.xmlfragment("zmm_create_pharm_or.view.popovers." + sName + 'Popover', this);
                    this.getView().addDependent(this[sName]);
                }
               
                if (sFrom === 'table'){
                    oRow = oEvent.getSource().getBindingContext("JSON").getObject();
                    oComponent_PharmCr.getModel("JSON").setProperty("/SelectedRowPopover", oRow);

                }else if(!!sFrom){
                    oRow = oComponent_PharmCr.getModel("JSON").getProperty("/SelectedItem");
                    oComponent_PharmCr.getModel("JSON").setProperty("/SelectedRowPopover", oRow);
                }

                if(sName === 'info'){
                    oComponent_PharmCr.getModel("JSON").setProperty("/infoTitlePop", sTitle);
                    oComponent_PharmCr.getModel("JSON").setProperty("/infoBodyPop", sBody);

                }

                this[sName].openBy(oEvent.getSource());
                
            },
            showErrorsMsg: function (aMessages) {
                oComponent_PharmCr.getModel("JSON").setProperty("/Messages", aMessages);
                this.onOpenMessageViewDialog();

            },
            onOpenMessageViewDialog: function (oEvent) {
                if (!oComponent_PharmCr._MessageView) {
                    oComponent_PharmCr._MessageView = sap.ui.xmlfragment("zmm_create_pharm_or.view.popovers." + 'MessageViewDialog', this);
                    this.getView().addDependent(oComponent_PharmCr._MessageView);
                }
                oComponent_PharmCr._MessageView.open();
            },
            afterMessageViewClose: function (oEvent) {
                oComponent_PharmCr._MessageView.destroy();
                oComponent_PharmCr._MessageView = undefined;

                var oModel = oComponent_PharmCr.getModel("JSON"),
                    bSuccCreatOrder = oModel.getProperty("/bSuccCreatOrder"),
                    bSuccessReleaseAndNote = oModel.getProperty("/bSuccessReleaseAndNote"),
                    sType = oModel.getProperty("/sTypeCreateOrder");

                if(bSuccCreatOrder){ //there isn`t errorMsg in create order
                    //var ordersNumbers = oModel.getProperty("/ordersNumbers");
                    if(sType === '1'){
                        var navCon = sap.ui.getCore().byId("navCon");
                        navCon.to("page2", "Slide");
                    }else if(sType === '2'){
                        this.onNavBack();
                    }

                }else if(bSuccessReleaseAndNote){ //there isn`t errorMsg in NoteAndRealeaseOrderHeaderSet
                    this.SaveAndSend(oEvent);
                }
            },
            onNavBack: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                oCrossAppNavigator.toExternal({
                    target: { shellHash: "#" }
                });

            },
            handleSortDialogConfirm: function (oEvent) {
                var oTable = this.byId("dataTable"),
                    mParams = oEvent.getParameters(),
                    oBinding = oTable.getBinding("items"),
                    sPath,
                    bDescending,
                    aSorters = [];
                try {
                    sPath = mParams.sortItem.getKey();
                    bDescending = mParams.sortDescending;
                    aSorters.push(new Sorter({ path: sPath, descending: bDescending }));

                } catch (e) { }


                // apply the selected sort and group settings
                oBinding.sort(aSorters);
            },
            JumpToNextInput: function(event, nextOne ,index, jumpCount, sInputId, errorValue) {
                // var oJsonModel = oComponent_PharmCr.getModel("JSON"),
                //     sPath = event.getSource().getBindingContext("JSON").getPath(),
                //     sValue = event.getSource().getValue(),
                //     maxQuan = Number(oJsonModel.getProperty(sPath + '/Maxqut')),
                //     roundCount = Number(oJsonModel.getProperty(sPath + '/Bstrf'));
                    
                // if( Number(sValue) > maxQuan && maxQuan){
                //     MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("MaxCount", maxQuan), {
                //         duration: 2000
                //     });
                // }else if( Number(sValue) % roundCount !== 0 && roundCount){
                //     MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("RoundCount", roundCount), {
                //         duration: 2000
                //     });
                // }
                try{
                    var currentId = !!event ? event.getSource().getId() : '',
                        //currentId.replace("__input1-__clone", "")
                        sInputId =  !!currentId ? currentId.substr(0,16) : sInputId,
                        index = index || parseInt(currentId.split('clone')[1]),
                        nextOne = nextOne || (!!sInputId ? sap.ui.getCore().byId(sInputId + (index+jumpCount)) : null );

                    if(!errorValue){ //אם ערך השדה לא תקין
                        if(!nextOne){
                            nextOne = sap.ui.getCore().byId(sInputId + (index+jumpCount+1));
                        }
                        else if(!sap.ui.getCore().byId(nextOne.getId()).getEnabled()){
                            this.JumpToNextInput("" , sap.ui.getCore().byId(sInputId + (index+jumpCount)), index+jumpCount, jumpCount, sInputId);
                        }
                        nextOne.focus();
                        nextOne._doSelect();
                    }

                  
                //$("#__input6-__clone" + (index+11))[0].focus(); // This is Ivan's Idea
                }catch (e){
                    // end of lines kanire
                }
            },
            selectAllTableItems: function (oEvent) {
                var table = oComponent_PharmCr._MasterController.getView().byId("dataTable"),
                allItems = table.getBinding("rows").aIndices,
                selected = oEvent.getParameter("selected");
                for (var i = 0; i < allItems.length; i++) {
                    var item = allItems[i],
                        rowItem = oComponent_PharmCr.getModel("JSON").getProperty('/items/' + item);
                    // var path = allItems[i].getPath();
                    if(!rowItem.Block && !rowItem.InCart){ //check if material doesn`t in cart or doesn`t blocking
                        oComponent_PharmCr.getModel("JSON").setProperty('/items/' + item + '/Selected', selected);
                    }
                }
                // var allItems = oComponent_PharmCr._MasterController.getView().byId("dataTable").getBinding("rows").getContexts(),
                //     selected = oEvent.getParameter("selected");
                // for (var i = 0; i < allItems.length; i++) {
                //     var item = allItems[i].getObject();
                //     var path = allItems[i].getPath();

                //     if(!item.Block && !item.InCart){ //check if material doesn`t in cart or doesn`t blocking
                //         oComponent_PharmCr.getModel("JSON").setProperty(path + '/Selected', selected);
                //     }
                // }

            },
            onTableRowSelected: function (oEvent, items) {
                try {
                    oEvent.getSource().getBindingContext("JSON").getObject().Selected = oEvent.getParameter("selected");
                } catch (e) { }
            },
            onPressComment: function (oEvent) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject(),
                    oSelectedItemPath = oEvent.getSource().getBindingContext("JSON").getPath(),
                    oModel = oComponent_PharmCr.getModel("JSON");

                oModel.setProperty("/oSelectedRow", oSelectedItem);
                oModel.setProperty("/oSelectedRowPath", oSelectedItemPath);
                oModel.setProperty("/newItemText", oSelectedItem.PrComment);
                oModel.setProperty("/matnrDes", oSelectedItem.Matnr + " " + oSelectedItem.Maktx);

                this.onOpenDialog('', 'comment');
            },
            saveCommentRow: function (oEvent) {
                var oModel = oComponent_PharmCr.getModel("JSON"),
                    aItemsCart = oModel.getProperty("/CartItems"),
                    sTableItems = oModel.getProperty("/items"),
                    oRow = oModel.getProperty("/oSelectedRow"),
                    oRowPath = oModel.getProperty("/oSelectedRowPath"),
                    isOrderComment = oModel.getProperty("/isOrderComment");

                    oModel.setProperty(oRowPath + "/PrComment" , oModel.getProperty("/newItemText"));

                if(!isOrderComment){
                    for (var i = 0; i < aItemsCart.length; i++) {
                        for (var j = 0; j < sTableItems.length; j++) {
                            if (aItemsCart[i].Matnr === sTableItems[j].Matnr) {
                                sTableItems[j].PrComment = aItemsCart[i].PrComment;
                                break;
                            }
                        }
                    }
                }else{
                    oModel.getProperty("/isOrderComment",false);
                }
                
                oComponent_PharmCr.getModel("JSON").refresh();
                this.onCloseDialog('', 'comment');
            },
            onPressCommentOrder: function (oEvent) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject(),
                    oSelectedItemPath = oEvent.getSource().getBindingContext("JSON").getPath(),
                    oModel = oComponent_PharmCr.getModel("JSON");

                oModel.setProperty("/oSelectedRow", oSelectedItem);
                oModel.setProperty("/oSelectedRowPath", oSelectedItemPath);
                oModel.setProperty("/newItemText", oSelectedItem.PrComment ? oSelectedItem.PrComment : "");
                oModel.setProperty("/isOrderComment", true);
                oModel.setProperty("/matnrDes", oSelectedItem.Ebeln);

                this.onOpenDialog('', 'comment');
            },
            onBeforeSaveDraft : function (oEvent) {
                var	oJsonModel = oComponent_PharmCr.getModel("JSON"),
                    aTableItems =  oJsonModel.getProperty("/items"),
                    bItemsNotInCart = false;

                for (var j = 0; j < aTableItems.length; j++) {
                    if (aTableItems[j].Selected && !aTableItems[j].InCart) { //אם קיים פריט שמסומן אך לא נוסף לסל
                        bItemsNotInCart = true;
                        MessageBox.alert("שים לב, לפני שמירת הטיוטה יש להוסיף את כל הפריטים שנבחרו לסל", {
                            actions: [ 'חזור', 'המשך לשמירת הטיוטה'],
                            emphasizedAction: 'המשך לשמירת הטיוטה',
                            onClose: function (sAction) {
                                if (sAction === 'המשך לשמירת הטיוטה') { 
                                    oComponent_PharmCr._MasterController.onCreateOrder('2');
                                }
                            }
                        });
                        break;
                    }
                }
                if(!bItemsNotInCart){
                    MessageBox.confirm("האם תרצה לשמור את ההזמנה כטיוטה?", {
                        actions: ['ביטול', 'אישור'],
                        emphasizedAction: 'אישור',
                        onClose: function (sAction) {
                            if (sAction === 'אישור') {
                                oComponent_PharmCr._MasterController.onCreateOrder('2');
                            }
        
                        }
                    });
                }
    
            },
            onBeforeDeleteDraft : function (oEvent) {
                var oModelJson = oComponent_PharmCr.getModel("JSON");
                MessageBox.alert("שים לב שכל הפריטים בטיוטה יימחקו. האם ברצונך להמשיך?", {
                    actions: ['לא', 'כן'],
                    emphasizedAction: 'כן',
                    onClose: function (sAction) {
                        if (sAction === 'כן') {
                            oModelJson.setProperty("/fromDeleteDraft", true);
                            oComponent_PharmCr._MasterController.onCreateOrder('2');
                        }
                    }
                });
    
            },
            onPressEditCart: function (bEdit) {
                oComponent_PharmCr.getModel("JSON").setProperty("/editCart", !bEdit);
            },
            onBeforeCreateOrder: function(sType){
                if (!window.event.detail || window.event.detail == 1) { 
                    this.onCreateOrder(sType);  
                }
            },
            onCreateOrder: function (sType) {
                var oModel = oComponent_PharmCr.getModel("JSON").getData(),
                    oModelJson = oComponent_PharmCr.getModel("JSON"),
                    aItemsToSend = [],
                    items = oModel.items,
                    IsUpdateDraft = oModelJson.getProperty("/IsDraft"),
                    fromDeleteDraft = oModelJson.getProperty("/fromDeleteDraft"),
                   bValidMatnr = true;

                    //check validition of matnr - block or amount not valid 
                    bValidMatnr = this.checkValidMatnr(items);
                    if(!bValidMatnr)
                        return;
                    aItemsToSend = this.fillArrayItems(fromDeleteDraft);
                
                    var oEntry = {
                        IvDraft:  sType === '2' ? (IsUpdateDraft ? "U" : "N" ) : "",
                        PoErrorMessage: [],
                        PoList: [],
                        PoMatList: aItemsToSend,
                        PrList: []
                    }
                    models.createOrder(oEntry).then(function (data) {
                        var aMessage = data.PoErrorMessage.results,
                            allSuccess = true;
    
                        if (aMessage.length > 0) {
                            for(let oMess in aMessage){
                                if(aMessage[oMess]['Type'] === 'E'){
                                    allSuccess = false;
                                    break;
                                }
                            }
                            oModelJson.setProperty("/bSuccCreatOrder", allSuccess);
                            oModelJson.setProperty("/sTypeCreateOrder", sType);
    
                            if(allSuccess && sType === '1'){
                                oModelJson.setProperty("/ordersNumbers", data.PoList.results);
                            }
                            if(!allSuccess){
                                oComponent_PharmCr._MasterController.updateItemsFromPrList(data);
                            }
                            this.showErrorsMsg(aMessage);
    
                        }else if (sType === '1') { //יצירת הזמנה
    
                            oModelJson.setProperty("/ordersNumbers", data.PoList.results);
                            var navCon = sap.ui.getCore().byId("navCon");
                            navCon.to("page2", "Slide");
    
                            }else if (sType === '2') { //טיוטה
                                if(fromDeleteDraft){ //מחיקת טיוטה
                                    MessageBox.confirm("הטיוטה נמחקה בהצלחה", {
                                        actions: ['סגירה'],
                                        emphasizedAction: 'סגירה',
                                        onClose: function (sAction) {
                                            if (sAction === 'סגירה') {
                                                oComponent_PharmCr._MasterController.onNavBack();
                                        }}
                                    });
                                }else{ //שמירת טיוטה
                                    if(!IsUpdateDraft){ // אם לא הגענו ממסך בקרת הזמנות מעדכנים את הפריטים בסל במספר הדרישה על מנת לא ליצור טיוטה נוספת
                                        oComponent_PharmCr._MasterController.updateItemsFromPrList(data);
                                    }
                                    this.onOpenDialog("", 'SaveDraft');
                                }
                            }
                    }.bind(this)).catch(function (error) {
                        //console.log(error);
                        try {
                            MessageBox.error(JSON.parse(error.responseText).error.message.value);
                        } catch (e) {
                            MessageBox.error(JSON.stringify(error));
                        }
                    });
            },
            fillArrayItems: function(fromDeleteDraft){
                var aItemsToSend = [],
                    oModel = oComponent_PharmCr.getModel("JSON").getData(),
                    oModelJson = oComponent_PharmCr.getModel("JSON"),
                    items = oModel.items;

                if(fromDeleteDraft){
                    aItemsToSend.push({ Berid : oModel.Filters.Berid});
                }else{
                    for (var i = 0; i < items.length; i++) {
                        //בדיקה אם החומר נמצא בסל או אם הוא לא נמצא ומחוק(לעדכון החומר בטיוטה)
                        if ( (items[i].Selected && items[i].InCart) || (!items[i].Selected && !items[i].InCart && items[i].IsDeleted)) {
                            aItemsToSend.push({ 
                                Berid: items[i].Berid,
                                Lbtxt: items[i].Lbtxt,
                                ShigStock:  items[i].ShigStock,
                                Bertx: items[i].Bertx,
                                Atccode: items[i].Atccode,
                                Atccode7: "",
                                Atccode9:"",
                                Mtart: items[i].Mtart,
                                Mtbez: items[i].Mtbez,
                                Maabc: items[i].Maabc,
                                Labor: items[i].Labor,
                                Raube: items[i].Raube,
                                Matnr: items[i].Matnr,
                                Comment: items[i].Comment,
                                Maktx: items[i].Maktx,
                                Mstae: items[i].Mstae,
                                Mtstb: items[i].Mtstb,
                                Meins: items[i].Meins,
                                PoMenge: (items[i].OrderQuanInt).toString() || '0',
                                Labst: items[i].Labst,
                                OpenQty: items[i].OpenQty,
                                ZstockDays: items[i].ZstockDays,
                                LabstLog: items[i].LabstLog,
                                IssueMenge: items[i].IssueMenge,
                                Maxqut: items[i].Maxqut,
                                Bstrf: items[i].Bstrf,
                                Pharmprice:  items[i].Pharmprice,
                                Waers: items[i].Waers,
                                Delnr: items[i].Delnr,
                                Delps: items[i].Delps,
                                PrComment: items[i].PrComment,
                                Recomend: items[i].Recomend,
                                Unblocked: items[i].Unblocked,
                                Drugs: items[i].Drugs,
                                Favorite: items[i].Favorite,
                                LowStock: items[i].LowStock,
                                Direct: items[i].Direct,
                                Block: items[i].Block,
                                Atcflag: items[i].Atcflag,
                                FromDate: items[i].FromDate,
                                ToDate: items[i].ToDate,
                                RecomendSum: items[i].RecomendSum,
                                UnblockedSum: items[i].UnblockedSum,
                                FavoriteSum: items[i].FavoriteSum,
                                AllLinesSum: items[i].AllLinesSum,
                                IsAtc7: false,
                                IsAtc9: false,
                                IsAtc11 : false
                            });
                        }
                    }
                }
                return aItemsToSend;
            },
            updateItemsFromPrList: function (data){
                var oModelJson = oComponent_PharmCr.getModel("JSON"),
                    oModel = oModelJson.getData(),
                    aItems = oModel.items;

                if(!!data.PrList.results.length){
                       data.PrList.results.forEach(oPrList => {
                            for(var i = 0; i < aItems.length; i++){
                                if(aItems[i].InCart && oPrList.Matnr === aItems[i].Matnr ){
                                    aItems[i].Delnr = oPrList.Delnr;
                                    break;
                                }
                            }                                          
                        });
                }
                for(var i = 0; i < aItems.length; i++){
                    if(!!aItems[i].Delnr && aItems[i].IsDeleted){
                        aItems[i].Delnr = "";
                        aItems[i].IsDeleted = false;
                    }
                }     
                oModelJson.refresh(true);
            },
            onCloseSuccDraftDialog: function (oEvent){
                this.onCloseDialog(oEvent, 'SaveDraft');

                // var oJsonModel = oComponent_PharmCr.getModel("JSON"),
                //     sMrpArea = oJsonModel.getProperty("/Filters/Berid"),
                //     oParams = {IsDraft:true, MrpArea: sMrpArea};

                // oJsonModel.setProperty("/IsDraft", true);
                // oComponent_PharmCr._MasterController.oRouter.navTo("master", oParams);

                //this.onNavBack();

                // debugger;
                // var oJsonModel =  oComponent_PharmCr.getModel("JSON"),
                //     bIsDraft = oJsonModel.getProperty("/IsDraft");

                //   //  selectedTab = oJsonModel.getProperty("/IconTabBarKey");

                // if(bIsDraft){
                //     //oJsonModel.setProperty("/IconTabBarKey",selectedTab);
                //     oComponent_PharmCr._MasterController.getItems();
                // }else{
                //     this.oRouter.navTo("master", {IsDraft: true});
                // }
            },
            onFinishWithOutApprove: function (oEvent) {
                this.onOpenDialog(oEvent,'Finish');
            },
            onUpdateAndReleaseOrder: function (oEvent, toRelease) {
                var oModelJson = oComponent_PharmCr.getModel("JSON"),
                    bExistImpOrd = false,
                    aOrdersList = oModelJson.getProperty("/ordersNumbers"),
                    aOrderToSend = [];
                if(!toRelease){
                    this.onCloseDialog(oEvent, 'Finish');
                }
                for (const key in aOrdersList) {
                        const element = aOrdersList[key];
                        aOrderToSend.push({
                            OrderNum: element.Ebeln,
                            OrderHeaderText: element.PrComment
                        })
                }
                for(const obj of aOrdersList){
                    if(obj.Iswf){
                        bExistImpOrd = true;
                        break;
                    }                       
                }
                var oEntry = {
                    IvDummy: true,
                    IvRelease: toRelease,
                    MessageReturn: [],
                    NoteAndRealeaseOrderErrors: [],
                    NoteAndRealeaseOrderNotes: aOrderToSend
                }
                models.updateAndReleaseOrder(oEntry).then(function (data) {
                    var aMessage = data.MessageReturn.results,
                        allSuccess = true;

                    if (aMessage.length > 0) {
                        for(let oMess in aMessage){
                            if(aMessage[oMess]['Type'] === 'E'){
                                allSuccess = false;
                                break;
                            }
                        }
                        oModelJson.setProperty("/bSuccessReleaseAndNote", allSuccess);
                        this.showErrorsMsg(aMessage);
                    }else{
                        if(bExistImpOrd && toRelease){ //אם קיימות הזמנות מיוחדות
                            MessageBox.warning(oComponent_PharmCr.i18n("navToMail"), {
                                actions: ['צא', 'דואר נכנס'],
                                title: oComponent_PharmCr.i18n("note"),
                                emphasizedAction: 'דואר נכנס',
                                onClose: function (sAction) {
                                    if (sAction === 'דואר נכנס') {
                                        oComponent_PharmCr._MasterController.onCloseDialog(oEvent, 'Cart');
                                        oComponent_PharmCr._MasterController.GoToTransaction(oEvent , 'WorkflowTask');
                                    } else {
                                        oComponent_PharmCr._MasterController.SaveAndSend(oEvent);
                                    }
                                }
                            }).catch((error) => {
                                models.handleErrors(error);
                            });

                        }else{
                            this.SaveAndSend(oEvent);
                        }
                    }
                }.bind(this)).catch(function (error) {
                    //console.log(error);
                    // try {
                    //     responseText).error.message.value);
                    // } catch (e) {
                    //     MessageBox.error(JSON.stringify(error));
                    // }
                });
            },
            onCloseApproveDialog: function(oEvent){
                this.onCloseDialog(oEvent, 'Cart');
                this.onNavBack();
            },
            SaveAndSend: function (oEvent) {
                this.onCloseDialog(oEvent, 'Cart');
                this.onNavBack();
            },
            enterOrderCount: function (oEvent, sType) {
                var oJsonModel = oComponent_PharmCr.getModel("JSON").getData(),
                    allItems = sType === 'genric' ? oJsonModel.GenItems : oJsonModel.items,
                    TotalPrice = 0,
                    oRow = oEvent.getSource().getBindingContext("JSON").getObject();
                oRow.OrderQun = parseInt(oRow.OrderQuan);
                oRow.TotalPrice = parseInt(Number(oRow.OrderQuan)) * Number(oRow.NetPrice);
                oComponent_PharmCr.getModel("JSON").refresh();
                for (var i = 0; i < allItems.length; i++) {
                    if (allItems[i].Selected) {
                        TotalPrice += parseInt(Number(allItems[i].OrderQuan)) * Number(allItems[i].NetPrice);
                    }
                }
                if (sType !== 'genric') {
                    oComponent_PharmCr.getModel("JSON").setProperty("/TotalPrice", TotalPrice.toFixed(2));
                }

            },
            
            GoToTransaction: function (event, sType, value1, value2) {
                var semanticObject, action, oParams,
                    oModelData = oComponent_PharmCr.getModel("JSON").getData().Filters;

                    if(value2){ 
                        var value2StartChar = value2.slice(-4,1), //האות הראשונה
                            value2Plus1 = value2StartChar + (Number(value2.substr(value2.length - 3)) + 1 ).toString(),
                            value2Plus2 = value2StartChar + (Number(value2.substr(value2.length - 3)) + 2 ).toString();
                    }
                switch (sType) {
                    case 'WorkflowTask':
                        semanticObject = 'WorkflowTask';
                        action = 'displayInbox';
                        oParams = {
                            allItems: true
                        };
                        break;
                    case 'MM03':
                        semanticObject = 'MM03';
                        action = 'display';
                        oParams = {
                            IvMaterial: value1
                        };
                        break;
                    case 'MMPURPAMEPO':
                        semanticObject = 'MMPURPAMEPO';
                            action = 'display';
                            oParams = {
                                PurchaseOrder: value1
                            };
                        break;
                    case 'ME23N':
                        semanticObject = 'ME23N';
                        action = 'display';
                        oParams = {
                            IvOrder: value1
                        };
                        break;
                    case 'Material':
                        semanticObject = 'Material';
                        action = 'displayStockMultipleMaterials';
                        oParams = {
                            Material: value1,
                            Plant: '1000',
                            StorageLocation: [value2, value2Plus1, value2Plus2]
                        };
                        break;
                    case 'MaterialMovement':
                        semanticObject = 'MaterialMovementNew';
                        action = 'displayList';
                        oParams = {
                            Material: value1,
                            Plant: '1000',
                            StorageLocation: [value2],
                            GoodsMovementType: ['Z21', 'Z22'],
                            StockChangeType: '05'
                        };
                        break;

                }
                sType === 'WorkflowTask' ?  this.crossAppNav(event, semanticObject, action, oParams) :  this.navToTransaction(event, semanticObject, action, oParams);
            },
            goToApp: function (oEvent, orderNum) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "zmm_create_pharm_or-create";
                var actionURL = "create&/c/" + orderNum;
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: 'zmm_create_pharm_or',
                                    action: actionURL
                                }
                            })) || "";
                            sap.m.URLHelper.redirect(hash, true);
                        }

                    })
                    .fail(function () { });
            },
            navToTransaction: function (event, sSemanticObject, sAction, oParams) {

                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = sSemanticObject + '-' + sAction;
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: sSemanticObject,
                                    action: sAction
                                },
                                params: oParams
                            })) || "";
                            sap.m.URLHelper.redirect(hash, true);
                        }

                    })
                    .fail(function () { });
            },
            crossAppNav: function (event, sSemanticObject, sAction, oParams) {

                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = sSemanticObject + '-' + sAction;
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: sSemanticObject,
                                    action: sAction
                                },
                                params: oParams
                            })) || "";
                            sap.m.URLHelper.redirect(hash, false);
                        }

                    })
                    .fail(function () { });
            },
            onSaveChangeCart: function (oEvent) {
                var aItems = oComponent_PharmCr.getModel("JSON").getProperty("/CartItems"),
                    aDeletedItems = oComponent_PharmCr.getModel("JSON").getProperty("/deletedItems"),
                    sCartTotalPrice = oComponent_PharmCr.getModel("JSON").getProperty("/cartTotalPrice"),
                    OrderQuanValid = true;
                
                for (var i = 0; i < aItems.length; i++) {
                    OrderQuanValid = this.checkOrderQuan(aItems[i], true);
                    if(!OrderQuanValid)
                        return;
                }
                oComponent_PharmCr.getModel("JSON").setProperty("/saveChangeInCart", true);
                oComponent_PharmCr.getModel("JSON").setProperty("/oldCartItems", jQuery.extend(true, [], aItems));
                oComponent_PharmCr.getModel("JSON").setProperty("/oldCartTotalPrice", sCartTotalPrice);
                var sTableItems = oComponent_PharmCr.getModel("JSON").getProperty("/items");

                for (var i = 0; i < aItems.length; i++) {
                    for (var j = 0; j < sTableItems.length; j++) {
                        if (aItems[i].Matnr === sTableItems[j].Matnr) {
                            sTableItems[j].OrderQuan = aItems[i].OrderQuan;
                            sTableItems[j].OrderQuanInt = aItems[i].OrderQuanInt;
                            sTableItems[j].TotalPrice = aItems[i].TotalPrice;
                            sTableItems[j].PrComment = aItems[i].PrComment;
                            break;
                        }
                    }
                }
                for (var i = 0; i < aDeletedItems.length; i++) {
                    for (var j = 0; j < sTableItems.length; j++) {
                        if (aDeletedItems[i].Matnr === sTableItems[j].Matnr) {
                            sTableItems[j].Selected = false;
                            sTableItems[j].InCart = false;
                            sTableItems[j].OrderQuanInt = 0;
                            sTableItems[j].OrderQuan = '0';

                            if(!!sTableItems[j].Delnr){ //קיימת דרישה עבור החומר 
                                sTableItems[j].IsDeleted = true;         
                            }
                            break;
                        }
                    }
                }
                oComponent_PharmCr.getModel("JSON").setProperty("/deletedItems",[]),

                oComponent_PharmCr.getModel("JSON").refresh(true);
                oComponent_PharmCr.getModel("JSON").setProperty("/TotalPrice", sCartTotalPrice);
                oComponent_PharmCr.getModel("JSON").setProperty("/CartCount", aItems.length);
                oComponent_PharmCr.getModel("JSON").setProperty("/editCart", false);
            },
            checkValidMatnr: function (aItem){
                var aMessage = [],
                    aNotValidMatnrs = [],
                    bItemValid = true,
                    modelJson = oComponent_PharmCr.getModel("JSON"),
                    aCartItems = modelJson.getProperty("/CartItems");


                for (var i = 0; i < aItem.length; i++) {
                    if(aItem[i].Selected && aItem[i].InCart){
                        if(Number(aItem[i].OrderQuan) > Number(aItem[i].Maxqut) && Number(aItem[i].Maxqut) ) {
                            aMessage.push({Message: oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("MaxCountWithNumMsg" ,[aItem[i].Matnr, +(aItem[i].Maxqut).toString()]),
                                           Type:'W'
                                        });
                            bItemValid = false;
                        }
                        if( !!(Number(aItem[i].OrderQuan) / Number(aItem[i].Bstrf)).toString().split(".")[1] && Number(aItem[i].Bstrf)) {
                            aMessage.push({Message: oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("RoundCountWithNumMsg" ,[aItem[i].Matnr, +(aItem[i].Bstrf).toString()]),
                                           Type: 'W'
                                        });
                            bItemValid = false;
                        }
                        if(aItem[i].Block){
                            aMessage.push({Message: oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("BlockWithNumMsg" ,aItem[i].Matnr),
                                          Type:'W'
                                        });
                            bItemValid = false;
                        }

                        if(!bItemValid){
                            aNotValidMatnrs.push({Matnr: aItem[i].Matnr});
                            bItemValid = true;
                        }
                    
                    }
                }

                if(!!aMessage.length){
                    this.showErrorsMsg(aMessage);

                    aNotValidMatnrs.forEach(notValidObj => {
                        for(var i = 0; i < aCartItems.length; i++){
                            if(aCartItems[i].Matnr === notValidObj.Matnr){
                                modelJson.setProperty("/CartItems/" + i + "/notValid", true);
                            }
                        }
                    });   
                    modelJson.refresh();           
                    return false;
                }else{
                    return true;
                }
            },
            checkOrderQuan: function (item, showMsg) {
                if (Number(item.OrderQuan) === 0) {
                    if(showMsg){
                        MessageToast.show(oComponent_PharmCr.i18n('noCountMsg'), {
                            duration: 4000
                        });
                    }
                    return false;
                }else if(Number(item.OrderQuan) > Number(item.Maxqut) && Number(item.Maxqut) ) {
                    if(showMsg){
                        MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("MaxCountMsg"), {
                            duration: 4000
                        });
                     }
                    return false;
                }else if( !!(Number(item.OrderQuan) / Number(item.Bstrf)).toString().split(".")[1] && Number(item.Bstrf)) {
                    if(showMsg){
                        MessageToast.show(oComponent_PharmCr.getModel("i18n").getResourceBundle().getText("RoundCountMsg"), {
                            duration: 4000
                        });
                    }
                    return false;
                }
                else{
                    return true;
                }
            },
            onItemCountChange: function (oEvent) {
                var value = oEvent.getSource().getValue(),
                    oRow = oEvent.getSource().getBindingContext("JSON").getObject(),
                    TotalPrice = 0;

                oRow.OrderQuan = value;
                oRow.OrderQuanInt = +value;
                oRow.TotalPrice = +value * Number(oRow.NetPriceFloat);
                oComponent_PharmCr.getModel("JSON").refresh();
                
                var items = oComponent_PharmCr.getModel("JSON").getProperty("/CartItems");

                for (var i = 0; i < items.length; i++) {
                    TotalPrice += +(items[i].OrderQuan) * +(items[i].NetPriceFloat);
                }
                oComponent_PharmCr.getModel("JSON").setProperty("/cartTotalPrice", TotalPrice.toFixed(2));
            },
            onOpenVizFrameDialog: function (oEvent, sDialogName) {
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment('vizFrameDialog', "zmm_create_pharm_or.view.popovers." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                var _graphDialog = sap.ui.core.Fragment.byId("vizFrameDialog", "idVizFrameDialog");
                oComponent_PharmCr._detailController.createFrameDialog(_graphDialog);
                this[sDialogName].open();
            },
            getDataOfTabs: function(sCurrTab,sMaterialNumber,sMrpArea,sFromDate,sToDate){
				switch (sCurrTab) {
					case 'MaterialDetails':
						models.LoadMatDetails(sMaterialNumber, sMrpArea.substr(sMrpArea.length - 4)).then((data) => {
							oComponent_PharmCr.getModel("JSON").setProperty("/MatDetails", data);
                            oComponent_PharmCr.getModel("JSON").refresh();			

						}).catch((error) => {
							models.handleErrors(error);
						});
						 break;
						 
					case 'MrpDetails':
						models.LoadMrpDetails(sMaterialNumber, sMrpArea).then((data) => {
							oComponent_PharmCr.getModel("JSON").setProperty("/MrpDetails", data);
                            oComponent_PharmCr.getModel("JSON").refresh();			

						}).catch((error) => {
							models.handleErrors(error);
						});
	
						break;
					case 'Sales':
						models.LoadSalesDetails(new Date(sFromDate), new Date(sToDate), sMaterialNumber, sMrpArea.substr(sMrpArea.length - 4)).then((data) => {
                            if(data.ItemSalesLine.results){
                                for (var i = 0; i < data.ItemSalesLine.results.length; i++) {
                                    data.ItemSalesLine.results[i].Quntity = parseInt(data.ItemSalesLine.results[i].Quntity);							
                                }
        
                                oComponent_PharmCr.getModel("JSON").setProperty("/SalesDetails", data);			
                                oComponent_PharmCr.getModel("JSON").refresh();			
                                oComponent_PharmCr._detailController.buildGraph();
                            }
						
						}).catch((error) => {
							models.handleErrors(error);
						});
	
						break;
	
					case 'orderHistory':
						models.LoadOrderHistory(sMaterialNumber, sMrpArea.substr(sMrpArea.length - 4) ).then((data) => {			
                            var aOrdersHstOrders = data.OrdersHstOrders.results,
                            aOrdersHstGoodsRecipt = data.OrdersHstOrders.results.length ?  data.OrdersHstOrders.results[0].OrdersHstGoodsRecipt.results : [];
                               
                            for (var j = 0; j < aOrdersHstOrders.length; j++) {
                                aOrdersHstOrders[j].GrList = [];
                            }
                            //התאמת קבלת הזמנה בהתאם למספר הזמנה
                            for (var i = 0; i < aOrdersHstGoodsRecipt.length; i++) {
                                for (j = 0; j < aOrdersHstOrders.length; j++) {
                                    if (aOrdersHstGoodsRecipt[i].OrderNum === aOrdersHstOrders[j].OrderNum && aOrdersHstGoodsRecipt[i].OrderItem === aOrdersHstOrders[j].OrderItem ) {
                                        aOrdersHstOrders[j].GrList.push(aOrdersHstGoodsRecipt[i]);
                                        break;
                                    }
                                }
                            }

							try {
                                //to delete the line in last location
                                data.OrdersHstOrders.results[data.OrdersHstOrders.results.length - 1].last = true;
							} catch (e) {}
	
							oComponent_PharmCr.getModel("JSON").setProperty("/OrderHistory", data);
                            oComponent_PharmCr.getModel("JSON").refresh();			

						
	
						}).catch((error) => {
							models.handleErrors(error);
						});
	
					break;
				}
			},
            onPressDeleteItem: function (oEvent) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject();
                var oPath = oEvent.getSource().getBindingContext("JSON").getPath();
                oComponent_PharmCr.getModel("JSON").setProperty("/deleteItem", oSelectedItem);
                oComponent_PharmCr.getModel("JSON").setProperty("/deleteItemPath", oPath);
                this.onOpenDialog('', 'DeleteItem')
            },
            onCancelDeleteItem: function (oEvent) {
                oComponent_PharmCr.getModel("JSON").setProperty("/deleteItem", '');
                this.onCloseDialog('', 'DeleteItem');
            },
            onDeleteItem: function (oEvent) {
                var model = oComponent_PharmCr.getModel("JSON");
                var modelData = model.getData();
                var cartItems = modelData.CartItems;
                let aPath = model.getProperty("/deleteItemPath").split("/");
                let indexToDelete = aPath[aPath.length - 1];
                var deletedItems = modelData.deletedItems;
                deletedItems.push(modelData.deleteItem);
                cartItems.splice(indexToDelete, 1);
                modelData.cartTotalPrice = this.onSumTableChanged(modelData.CartItems);
                model.refresh(true);
                this.onCloseDialog('', 'DeleteItem');
            },
            onSumTableChanged: function (items) {
                var TotalPrice = 0;
                for (var i = 0; i < items.length; i++) {
                    if (items[i].Selected) {
                        TotalPrice += items[i].OrderQuanInt * items[i].NetPriceFloat;
                    }
                }
                return TotalPrice.toFixed(2);
            },
            onCancelEditCart: function (oEvent) {
                var aOldItems = oComponent_PharmCr.getModel("JSON").getProperty("/oldCartItems");
                var sCartTotalPrice = oComponent_PharmCr.getModel("JSON").getProperty("/oldCartTotalPrice");
                oComponent_PharmCr.getModel("JSON").setProperty("/CartItems", jQuery.extend(true, [], aOldItems));
                oComponent_PharmCr.getModel("JSON").setProperty("/cartTotalPrice", sCartTotalPrice);
                oComponent_PharmCr.getModel("JSON").setProperty("/editCart", false);
            },
            onSearch: function (oEvent) {
                var sQuery = oEvent.getParameter("newValue");
    
                if (sQuery && sQuery.length > 0) {
                    var oFilter = new Filter({
                        filters: [	new Filter('Maktx', FilterOperator.Contains, sQuery),
                                    new Filter('Matnr', FilterOperator.Contains, sQuery),
                                    new Filter('Atccode', FilterOperator.Contains, sQuery)
    
                        ],
                        and: false
                    });
                }
                 oComponent_PharmCr._MasterController.getView().byId("dataTable").getBinding("rows").filter(oFilter, "Application");
            },
            openDraftExsitDialog: function () {
                debugger;
                MessageBox.alert("שים לב כי קיימת עבורך טיוטה", {
                    actions: [ 'המשך ליצירת הזמנה', 'עבור לבקרת הזמנות'],
                    emphasizedAction: 'המשך ליצירת הזמנה',
                    onClose: function (sAction) {
                        if (sAction === 'עבור לבקרת הזמנות') { 
                            var oJsonModel = oComponent_PharmCr.getModel("JSON"),
                            sMrpArea = oJsonModel.getProperty("/Filters/Berid");
                            oComponent_PharmCr._MasterController.crossAppNav(null,'zmm_pharm_report', 'display', { "MrpArea": sMrpArea});
                        }
                    }
                });
            },
            overrideUiTableFilter: function(sId){
                var columns = sId === "dataTable" ? oComponent_PharmCr._MasterController.byId(sId).getAggregation("columns") : 
                                                    sap.ui.getCore().byId("genericTable").getAggregation("columns");
    
                for (const col of columns){
                    col._parseFilterValue = function(sValue) {
                        var oFilterType = this.getFilterType();
                
                        if (oFilterType) {
                            if (jQuery.isFunction(oFilterType)) {
                                sValue = oFilterType(sValue);
                            } else {
                                sValue = oFilterType.parseValue(sValue, "string");
                            }
                        }
                        if(isNaN(sValue)){
                            return sValue;
                        }else{
                            return parseFloat(sValue);
                        }
                    };
                 
                }
            },
          
            
                
    });
    }
);

