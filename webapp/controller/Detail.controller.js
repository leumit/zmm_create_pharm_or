/* global oComponent_PharmCr: true */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"zmm_create_pharm_or/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"zmm_create_pharm_or/model/formatter",
	"zmm_create_pharm_or/model/models",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/viz/ui5/format/ChartFormatter',
	'sap/m/MessageToast',
	"sap/m/MessageBox"
], function (Controller,
	BaseController,
	JSONModel,
	formatter,
	models,
	Filter,
	FilterOperator,
	ChartFormatter,
	MessageToast,
	MessageBox) {
	"use strict";

	return BaseController.extend("zmm_create_pharm_or.controller.Detail", {

		formatter: formatter,
		onInit: function () {
			oComponent_PharmCr._detailController = this;

			var oExitButton = this.getView().byId("exitFullScreenBtn"),
				oEnterButton = this.getView().byId("enterFullScreenBtn");

			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();

			this.oRouter.getRoute("detail").attachPatternMatched(this._onDetailMatched, this);
			this.oRouter.getRoute("detailFull").attachPatternMatched(this._onFullMatched, this);

		},
		handleItemPress: function (oEvent) {
			var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(2),
				supplierPath = oEvent.getSource().getBindingContext("products").getPath(),
				supplier = supplierPath.split("/").slice(-1).pop();

			this.oRouter.navTo("detailDetail", {
				OrderNo: this._product,
				supplier: supplier
			});
		},
		handleFullScreen: function (sMaterial) {
			this.bFocusFullScreenButton = true;
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen"),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				SelectedTab = oJsonModel.getProperty("/Routes/SelectedTab"),
				sMrpArea = oJsonModel.getProperty("/Routes/sMrpArea"),
				sFromDate = oJsonModel.getProperty("/Routes/sFromDate").toISOString().split('T')[0] ,
				sToDate = oJsonModel.getProperty("/Routes/sToDate").toISOString().split('T')[0];

			this.oRouter.navTo("detailFull", {
				Material: sMaterial,
				MrpArea: sMrpArea,
				FromDate: sFromDate,
				ToDate: sToDate,
				tab: SelectedTab
			});
		},
		handleExitFullScreen: function (sMaterial) {
			this.bFocusFullScreenButton = true;
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen"),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				SelectedTab = oJsonModel.getProperty("/Routes/SelectedTab"),
				sMrpArea = oJsonModel.getProperty("/Routes/sMrpArea"),
				sFromDate = oJsonModel.getProperty("/Routes/sFromDate").toISOString().split('T')[0] ,
				sToDate = oJsonModel.getProperty("/Routes/sToDate").toISOString().split('T')[0];

			this.oRouter.navTo("detail", {
				Material: sMaterial,
				MrpArea: sMrpArea,
				FromDate: sFromDate,
				ToDate: sToDate,
				tab: SelectedTab
			});
		},
		handleClose: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn"),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				IsDraft = oJsonModel.getProperty("/IsDraft"),
				sMrpArea = oJsonModel.getProperty("/Filters/Berid"),
				oParams = !!IsDraft ? {IsDraft:true, MrpArea: sMrpArea} : null;


			oJsonModel.setProperty("/layout", 'OneColumn');
			oComponent_PharmCr._fcl.byId("fcl").setLayout("OneColumn");

			this.oRouter.navTo("master", oParams);
		},
		_onFullMatched: function (oEvent) {
			oComponent_PharmCr.getModel("JSON").setProperty("/layout", 'MidColumnFullScreen');
			oComponent_PharmCr._fcl.byId("fcl").setLayout("MidColumnFullScreen");
			var oJsonModel = oComponent_PharmCr.getModel("JSON"),
				items = oJsonModel.getProperty("/items"),
				IsDraft = oJsonModel.getProperty("/IsDraft"),
				sMrpArea = oJsonModel.getProperty("/Filters/Berid"),
				oParams = !!IsDraft ? {IsDraft:true, MrpArea: sMrpArea} : null;
				
			if (!items) {
				this.oRouter.navTo("master", oParams);
			}
		},
		_onDetailDetailMatched: function (oEvent) {
			oComponent_PharmCr.getModel("JSON").setProperty("/layout", 'TwoColumnsMidExpanded');
			
		},
		_onDetailMatched: function (oEvent) {
			var arg = oEvent.getParameter("arguments"),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				sMaterialNumber = arg["Material"],
				sMrpArea = arg['MrpArea'],
				sFromDate =  new Date(arg['FromDate']),
				sToDate =  new Date(arg['ToDate']),
				sCurrTab = arg['tab'],
				items = oJsonModel.getProperty("/items");

				if (!items) {
					oJsonModel.setProperty("/IconTabBarKey", "RecItems");
					oJsonModel.setProperty("/allTabSelected", false);
					oJsonModel.setProperty("/genericClick", false);
					oComponent_PharmCr._MasterController.getItems();
				}


			oJsonModel.setProperty("/Routes/SelectedTab",  sCurrTab ? sCurrTab : "MaterialDetails");
			oJsonModel.setProperty("/Routes/sMaterialNumber",  sMaterialNumber);
			oJsonModel.setProperty("/Routes/sMrpArea", sMrpArea);
			oJsonModel.setProperty("/Routes/sFromDate", sFromDate);
			oJsonModel.setProperty("/Routes/sToDate", sToDate);
			oJsonModel.setProperty("/layout", 'TwoColumnsMidExpanded');
			oComponent_PharmCr._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_PharmCr._fcl._updateUIElements();

			this.getDataOfTabs(sCurrTab,sMaterialNumber,sMrpArea,sFromDate,sToDate);
		
		},
		onDetailsTabSelected: function (oEvent) {
			var sKey = oEvent.getParameter("key"),
				oJsonModel = oComponent_PharmCr.getModel("JSON"),
				oModelData = oComponent_PharmCr.getModel("JSON").getData(),
				sMaterialNumber = oJsonModel.getProperty("/Routes/sMaterialNumber"),
				sMrpArea = oJsonModel.getProperty("/Routes/sMrpArea"),
				sFromDate = oJsonModel.getProperty("/Routes/sFromDate"),
				sToDate =  oJsonModel.getProperty("/Routes/sToDate");

				this.getDataOfTabs(sKey,sMaterialNumber,sMrpArea,sFromDate,sToDate);


				// navObj = {
				// 	Material: sMaterialNumber,				
				// 	MrpArea: sMrpArea,
				// 	FromDate: sFromDate,
				// 	ToDate: sToDate,
				// 	tab: sKey
				// };
				//TO CHANGE
				// navObj = {
				// 	Material: '1197',				
				// 	MrpArea: 'M0502C100',
				// 	FromDate: '2023-03-01',
				// 	ToDate: '2023-03-15',
				// 	tab: sKey
				// };

				//this.getRouter().navTo("detail", navObj);
		},
		buildGraph: function () {
			var _graph = this.byId("idVizFrame");
			var oProperties = {
				plotArea: {
					dataLabel: {
						visible: true,
						showTotal: true,
						hideWhenOverlap: false,
					},
					dataPointStyle: {
						"rules":
							[
								{
									"dataContext": { "Quntity": { "min": 0 } },
									"properties": {
										"color": "sapUiChartPaletteQualitativeHue1"

									},
									"displayName": "ניפוק"
								}
							]
					}, primaryScale: {
						fixedRange: true
					}
				},
				valueAxis: {
					title: {
						text: window.external.DataContext === undefined ? oComponent_PharmCr.i18n("unitsCount") : 'תודיחי תומכ'
					}
				},
				categoryAxis: {
					title: {
						text: window.external.DataContext === undefined ? oComponent_PharmCr.i18n("Date") : 'תאריך'
					}
				},
				title: {
					visible: true,
					text: '',
					alignment: "right"
				}
			};
			_graph.setVizProperties(oProperties);
		},
		createFrameDialog: function (graphDialog) {
			var oProperties = {
				plotArea: {
					dataLabel: {
						visible: true,
						showTotal: true,
						hideWhenOverlap: false,
					},
					dataPointStyle: {
						"rules":
							[
								{
									"dataContext": { "Quntity": { "min": 0 } },
									"properties": {
										"color": "sapUiChartPaletteQualitativeHue1"

									},
									"displayName": "ניפוק"
								}
							]
					}, primaryScale: {
						fixedRange: true
					}
				},
				valueAxis: {
					title: {
						text: window.external.DataContext === undefined ? oComponent_PharmCr.i18n("unitsCount") : 'תודיחי תומכ'
					}
				},
				categoryAxis: {
					title: {
						text: window.external.DataContext === undefined ? oComponent_PharmCr.i18n("Date") : 'תאריך'
					}
				},
				title: {
					visible: true,
					text: '',
					alignment: "right"
				}
			};
			graphDialog.setVizProperties(oProperties);

		},
		onOpenGrList: function (oEvent) {
			var event = jQuery.extend(true, {}, oEvent),
			items = jQuery.extend(true, [], oEvent.getSource().getBindingContext("JSON").getObject().GrList);
			items.splice(0, 3);
			oComponent_PharmCr.getModel("JSON").setProperty("/GrItems", items);
			this.onOpenPopover(event, 'History');

		},
		onClickFavorite: function(oEvent, isFavorite){
			var JsonModel = oComponent_PharmCr.getModel("JSON"),
				IvActivity = isFavorite ? "1" : "2",
				IvApplicationId = 'ZMM_CR_PRM_OR',
				IvObjectId = 'MATERIAL' ,
				IvObjectValue = JsonModel.getProperty("/SelectedItem/MatnrUnconv"),
				//IvObjectValue = '000000000010000005',
				IvUserName = '';

			models.UpdateFavoriteMat(IvActivity, IvApplicationId, IvObjectId ,IvObjectValue, IvUserName).then((data) => {
				if(data.EvSuccess){
					JsonModel.setProperty("/SelectedItem/Favorite", isFavorite);
					JsonModel.setProperty("/FavoriteClick", true);
				}
			}).catch((error) => {
				models.handleErrors(error);
			});
		}
	
	});
});