/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require(["zmm_create_pharm_or/test/integration/AllJourneys"], function () {
        QUnit.start();
    });
});
